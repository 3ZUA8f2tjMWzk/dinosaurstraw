///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import java . util . Map . Entry ;

class ParameterMapFunction extends AbstractParameterMapFunction < String , Object >
{
    private final String [ ] _keys ;

    private final Object [ ] _arguments ;

    ParameterMapFunction ( final String [ ] keys , final Object [ ] arguments )
	{
	    _keys = keys ;
	    _arguments = arguments ;
	}

    @ Override
	final String [ ] getKeys ( )
	{
	    return _keys ;
	}

    @ Override
	final Object [ ] getArguments ( )
	{
	    return _arguments ;
	}

    @ Override
	final Entry < String , Object > getEntry ( final String key , final String [ ] keys , final Object [ ] arguments )
	{
	    return new ParameterMapEntry ( key , keys , arguments ) ;
	}
}
