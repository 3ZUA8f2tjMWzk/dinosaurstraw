///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . BlueAutumn ;
import java . util . Collections ;
import java . util . Map ;
import javassist . util . proxy . MethodHandler ;
import javassist . util . proxy . ProxyFactory ;
import javassist . util . proxy . ProxyObject ;

public class DinosaurStrawFactory extends AbstractDinosaurStrawFactory
{
    @ Override
	final ProxyFactory proxyFactory ( )
    {
	return new ProxyFactory ( ) ;
    }

    @ Override
	final void setSuperclass ( final ProxyFactory proxyFactory , final Class < ? > superclass )
    {
	proxyFactory . setSuperclass ( superclass ) ;
    }

    @ Override
	final Class < ? > createClass ( final ProxyFactory proxyFactory )
    {
	return proxyFactory . createClass ( ) ;
    }

    @ Override
	@ BlueAutumn
	final < T > T newInstance ( final Class < ? extends T > clazz )
    {
	T instance = null ;
	try
	    {
		instance = clazz . newInstance ( ) ;
	    }
	catch ( final InstantiationException cause )
	    {
		throw new RuntimeException ( cause ) ;
	    }
	catch ( final IllegalAccessException cause )
	    {
		throw new RuntimeException ( cause ) ;
	    }
	return instance ;
    }

    @ Override
	final < T > T cast ( final Class < ? extends T > clazz , final Object object )
    {
	return clazz . cast ( object ) ;
    }

    @ Override
	final void setHandler ( final ProxyObject proxyObject , final MethodHandler methodHandler )
    {
	proxyObject . setHandler ( methodHandler ) ;
    }

    @ Override
	final Class < ? extends ProxyObject > proxyObjectClass ( )
    {
	return ProxyObject . class ;
    }

    @ Override
	final DinosaurStrawMethodHandler methodHandler ( final Map < String , Object > parameterMap )
    {
	return new DinosaurStrawMethodHandler ( parameterMap ) ;
    }

    @ Override
	final < K , V > Map < K , V > emptyMap ( )
    {
	return Collections . emptyMap ( ) ;
    }
}
