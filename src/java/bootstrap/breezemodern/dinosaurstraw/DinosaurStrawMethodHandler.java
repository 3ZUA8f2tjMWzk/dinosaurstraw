///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . UseArray ;
import breezemodern . easterncrayon . UseClassConstant ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseIntegerConstant ;
import breezemodern . easterncrayon . UseStaticMethod ;
import breezemodern . easterncrayon . UseStringConstant ;
import java . lang . annotation . Annotation ;
import java . lang . reflect . AccessibleObject ;
import java . lang . reflect . Array ;
import java . lang . reflect . Constructor ;
import java . lang . reflect . InvocationTargetException ;
import java . lang . reflect . Method ;
import java . lang . reflect . Modifier ;
import java . util . Arrays ;
import java . util . Map ;

class DinosaurStrawMethodHandler extends AbstractDinosaurStrawMethodHandler
{
    private static final Integer ZERO = 0 ;

    private static final Integer ONE = 1 ;

    private final Map < String , Object > _parametersMap ;

    DinosaurStrawMethodHandler ( final Map < String , Object > parametersMap )
    {
	_parametersMap = parametersMap ;
    }

    @ Override
	final UtilInterface util ( )
    {
	return new UtilImpl ( ) ;
    }

    @ Override
	final Integer zero ( )
    {
	return ZERO ;
    }

    @ Override
	final < A extends Annotation > A getAnnotation ( final Method proceed , final Class < A > clazz )
    {
	return proceed . getAnnotation ( clazz ) ;
    }

    @ Override
	final Class < ? extends UseStringConstant > useStringConstantClass ( )
    {
	return UseStringConstant . class ;
    }

    @ Override
	final String value ( final UseStringConstant useStringConstant )
    {
	return useStringConstant . value ( ) ;
    }

    @ Override
	final Class < ? extends UseClassConstant > useClassConstantClass ( )
    {
	return UseClassConstant . class ;
    }

    @ Override
	final Class < ? > value ( final UseClassConstant useClassConstant )
    {
	return useClassConstant . value ( ) ;
    }

    @ Override
	final Class < ? extends UseIntegerConstant > useIntegerConstantClass ( )
    {
	return UseIntegerConstant . class ;
    }

    @ Override
	final Integer value ( final UseIntegerConstant useIntegerConstant )
    {
	return useIntegerConstant . value ( ) ;
    }

    @ Override
	final Class < ? extends UseConstructor > useConstructorClass ( )
    {
	return UseConstructor . class ;
    }

    @ Override
	final Boolean isAbstract ( final Integer modifiers )
    {
	return Modifier . isAbstract ( modifiers ) ;
    }

    @ Override
	final Integer getModifiers ( final Class < ? > clazz )
    {
	return clazz . getModifiers ( ) ;
    }

    @ Override
	final AbstractDinosaurStrawFactory getDinosaurStrawFactory ( )
    {
	return new DinosaurStrawFactory ( ) ;
    }

    @ Override
	final < T > T make ( final AbstractDinosaurStrawFactory factory , final Class < ? extends T > clazz , final Map < String , Object > parametersMap )
    {
	return factory . make ( clazz , parametersMap ) ;
    }

    @ Override
	final Class < ? > value ( final UseConstructor useConstructor )
    {
	return useConstructor . value ( ) ;
    }

    @ Override
	final Class < ? > getReturnType ( final Method method )
    {
	return method . getReturnType ( ) ;
    }

    @ Override
	final < T > T newInstance ( final UtilInterface util , final Constructor < T > constructor , final Object [ ] args ) throws IllegalAccessException , InvocationTargetException
    {
	return util . newInstance ( constructor , args ) ;
    }

    @ Override
	final < T > Constructor < T > getConstructor ( final Class < T > clazz , final Class < ? > [ ] parameters ) throws NoSuchMethodException
    {
	return clazz . getConstructor ( parameters ) ;
    }

    @ Override
	final Map < String , Object > getParameterMap ( final Object [ ] args , final AccessibleObject method )
    {
	return new ParameterMap ( args , method ) ;
    }

    @ Override
	final Class < ? extends UseArray > useArrayClass ( )
    {
	return UseArray . class ;
    }

    @ Override
	final Class < ? extends UseStaticMethod > useStaticMethodClass ( )
    {
	return UseStaticMethod . class ;
    }

    @ Override
	final Class < ? > value ( final UseStaticMethod useStaticMethod )
    {
	return useStaticMethod . value ( ) ;
    }

    @ Override
	final Class < ? > [ ] getParameterTypes ( final Method method )
    {
	return method . getParameterTypes ( ) ;
    }

    @ Override
	final String getName ( final Method method )
    {
	return method . getName ( ) ;
    }

    @ Override
	final Method getMethod ( final Class < ? > clazz , final String name , final Class < ? > [ ] parameterTypes ) throws NoSuchMethodException
    {
	return clazz . getMethod ( name , parameterTypes ) ;
    }

    @ Override
	final Object invoke ( final Method method , final Object instance , final Object [ ] args ) throws IllegalAccessException , InvocationTargetException
    {
	return method . invoke ( instance , args ) ;
    }

    @ Override
	final Object get ( final Object [ ] array , final Integer index )
    {
	return Array . get ( array , index ) ;
    }

    @ Override
	final Integer one ( )
    {
	return ONE ;
    }

    @ Override
	final Integer getLength ( final Object [ ] array )
    {
	return Array . getLength ( array ) ;
    }

    @ Override
	final < T > T [ ] copyOfRange ( final T [ ] array , final Integer floor , final Integer ceiling )
    {
	return Arrays . copyOfRange ( array , floor , ceiling ) ;
    }

    @ Override
	final Class < ? > getClass ( final Object object )
    {
	return object . getClass ( ) ;
    }

    @ Override
	final Class < ? extends UseInstanceMethod > useInstanceMethodClass ( )
    {
	return UseInstanceMethod . class ;
    }

    @ Override
	final Map < String , Object > getParametersMap ( )
    {
	return _parametersMap ;
    }

    @ Override
	final Object get ( final Map < String , Object > parametersMap , final String key )
    {
	return parametersMap . get ( key ) ;
    }

    @ Override
	final < K > Boolean containsKey ( final Map < K , ? > map , final K key )
    {
	return map . containsKey ( key ) ;
    }

    @ Override
	final RuntimeException runtimeException ( )
    {
	return new RuntimeException ( ) ;
    }
}
