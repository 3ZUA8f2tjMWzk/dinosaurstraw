///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import java . util . List ;

class ParameterMapEntry extends AbstractParameterMapEntry < String , Object >
{
    private final String _key ;

    private final String [ ] _keys ;

    private final Object [ ] _arguments ;

    ParameterMapEntry ( final String key , final String [ ] keys , final Object [ ] arguments )
    {
	_key = key ;
	_keys = keys ;
	_arguments = arguments ;
    }

    @ Override
	public final String getKey ( )
    {
	return _key ;
    }

    @ Override
	final String [ ] getKeys ( )
    {
	return _keys ;
    }

    @ Override
	final StaticInterface staticInterface ( )
    {
	return new StaticImpl ( ) ;
    }

    @ Override
	final < T > List < T > asList ( final StaticInterface staticInterface , final T [ ] array )
    {
	return staticInterface . asList ( array ) ;
    }

    @ Override
	final < T > Integer indexOf ( final List < T > list , final T item )
    {
	return list . indexOf ( item ) ;
    }

    @ Override
	public Object [ ] getArguments ( )
    {
	return _arguments ;
    }

    @ Override
	final Object get ( final StaticInterface staticInterface , final Object array , final Integer index )
    {
	return staticInterface . get ( array , index ) ;
    }

    @ Override
	public final < T > T cast ( final Class < T > clazz , final Object object )
	{
	    return clazz . cast ( object ) ;
	}

    @ Override
	public final Class < Object > vClass ( )
	{
	    return Object . class ;
	}
}
