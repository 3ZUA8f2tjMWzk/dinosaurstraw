///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import com . thoughtworks . paranamer . AdaptiveParanamer ;
import com . thoughtworks . paranamer . Paranamer ;
import java . lang . reflect . AccessibleObject ;
import java . util . Collection ;
import java . util . List ;
import java . util . Set ;

class ParameterMap extends AbstractParameterMap < Object >
{
    private final Object [ ] _arguments ;

    private final AccessibleObject _method ;

    ParameterMap ( final Object [ ] arguments , final AccessibleObject method )
	{
	    _arguments = arguments ;
	    _method = method ;
	}

    @ Override
	final Object [ ] getArguments ( )
	{
	    return _arguments ;
	}

    @ Override
	final StaticInterface staticInterface ( )
	{
	    return new StaticImpl ( ) ;
	}

    @ Override
	final < T > List < T > asList ( final StaticInterface staticInterface , final T [ ] array )
	{
	    return staticInterface . asList ( array ) ;
	}

    @ Override
	final AccessibleObject getMethod ( )
	{
	    return _method ;
	}

    @ Override
	final Paranamer getParanamer ( )
	{
	    return new AdaptiveParanamer ( ) ;
	}

    @ Override
	final String [ ] lookupParameterNames ( final Paranamer paranamer , final AccessibleObject method )
	{
	    return paranamer . lookupParameterNames ( method ) ;
	}

    @ Override
	final Function < ? super String , ? extends Entry < String , Object > > getParameterMapFunction ( final String [ ] parameterNames , final Object [ ] arguments )
	{
	    return new ParameterMapFunction ( parameterNames , arguments ) ;
	}

    @ Override
	final < F , T > List < T > transform ( final StaticInterface staticInterface , final List < F > list , final Function < ? super F , ? extends T > function )
	{
	    return staticInterface . transform ( list , function ) ;
	}

    @ Override
	final < T > Set < T > getSet ( final StaticInterface staticInterface , final Collection < T > collection )
	{
	    return staticInterface . getSet ( collection ) ;
	}
}
