///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . easterncrayon . UseArray ;
import breezemodern . easterncrayon . UseClassConstant ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseIntegerConstant ;
import breezemodern . easterncrayon . UseStaticMethod ;
import breezemodern . easterncrayon . UseStringConstant ;
import java . io . IOException ;
import java . util . concurrent . Callable ;

import static breezemodern . blueautumn . Targets . INTEGER_CONSTANT ;
import static breezemodern . blueautumn . Targets . STRING_CONSTANT ;

public abstract class AnnotatedObjectTarget
{
    @ UseArray
	public abstract Parameter [ ] array ( Parameter ... parameters ) ;

    @ UseClassConstant ( ConcreteObjectTarget . class )
	public abstract Class < ? > classConstant ( ) ;

    @ UseConstructor ( ConstructorTarget . class )
	public abstract ConstructorTarget constructor ( ) ;

    @ UseInstanceMethod
	public abstract Object instanceMethod ( InstanceMethodTarget instance , Object input ) ;

    @ UseIntegerConstant ( INTEGER_CONSTANT )
	public abstract Object integerConstant ( ) ;

    @ UseStaticMethod ( StaticMethodTarget . class )
	public abstract Object staticMethod ( Object object ) ;

    @ UseStringConstant ( STRING_CONSTANT )
	public abstract Object stringConstant ( ) ;

    @ UseConstructor ( AnnotatedObjectTarget . class )
	public abstract AnnotatedObjectTarget annotatedObject ( ) ;

//CHECKSTYLE:OFF
    @ UseConstructor ( ParameterizedObjectTarget . class )
	public ParameterizedObjectTarget parameterizedObject ( final Object parameter )
//CHECKSTYLE:ON
    {
	return null ;
    }

    @ UseConstructor ( ChildObjectTarget . class )
	public abstract ParentObjectTarget abstractObjectTarget ( ) ;

    @ UseInstanceMethod
	public abstract Object identity ( ParentObjectTarget parent , Object object ) ;

//CHECKSTYLE:OFF
    @ UseConstructor ( CallableTarget . class )
	public Callable < ? > callableTarget ( final Object value )
//CHECKSTYLE:ON
    {
	return null ;
    }

    public final Object unannotated ( final Object parameter )
    {
	return parameter ;
    }

//CHECKSTYLE:OFF
    @ UseConstructor ( TargetInvocationExceptionTarget . class )
//CHECKSTYLE:ON
	public TargetInvocationExceptionTarget targetInvocationExceptionTarget ( )
    {
	return null ;
    }

    @ UseInstanceMethod
	public abstract Object legalException ( TargetInvocationExceptionTarget target ) throws IOException ;

    @ UseInstanceMethod
	public abstract Object illegalException ( TargetInvocationExceptionTarget target ) ;
}
