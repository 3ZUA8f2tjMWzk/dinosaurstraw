///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import java . io . IOException ;
import java . lang . annotation . Annotation ;
import java . sql . SQLException ;
import java . util . Map ;

import static java . util . Collections . emptyMap ;

public abstract class Targets
{
    public static final Class < ? extends Throwable > LEGAL_EXCEPTION_CLASS = IOException . class ;

    public static final IOException LEGAL_TARGET_EXCEPTION = new IOException ( ) ;

    public static final Class < ? extends Throwable > ILLEGAL_EXCEPTION_CLASS = SQLException . class ;

    public static final SQLException ILLEGAL_TARGET_EXCEPTION = new SQLException ( ) ;

    public static final Map < String , Object > EMPTY_PARAMETERS_MAP = emptyMap ( ) ;

    public static final Class < ? extends Annotation > ANNOTATION_CLASS = AnnotationTarget . class ;

    public static final Class < ? > ABSTRACT_OBJECT = AbstractObjectTarget . class ;

    public static final Class < ? > CONCRETE_OBJECT = ConcreteObjectTarget . class ;

    public static final Class < ? extends ParameterizedObjectTarget > PARAMETERIZED_OBJECT = ParameterizedObjectTarget . class ;

    public static final Class < ? extends AnnotatedObjectTarget > ANNOTATED_OBJECT = AnnotatedObjectTarget . class ;

    public static final String OBJECT_METHOD_NAME = "identity" ;

    public static final Class < ? > [ ] EMPTY_PARAMETER_TYPES = array ( Class . class ) ;

    public static final Object [ ] EMPTY_ARGUMENTS = array ( Object . class ) ;

    public static final Class < ? > [ ] SINGLE_PARAMETER_TYPES = array ( Class . class , Parameter . class ) ;

    public static final Parameter SINGLE_ARGUMENT = new Parameter ( ) ;

    public static final Object [ ] SINGLE_ARGUMENTS = array ( Parameter . class , SINGLE_ARGUMENT ) ;

    public static final Class INTERFACE_OBJECT = InterfaceTarget . class ;

    public static final Class < ? extends InstanceMethodTarget > INSTANCE_CLASS = InstanceMethodTarget . class ;

    public static final InstanceMethodTarget INSTANCE = new InstanceMethodTarget ( ) ;

    public static final Class < ? > NO_SUCH_METHOD_EXCEPTION = NoSuchMethodExceptionTarget . class ;

    public static final Class < ? > INSTANTIATION_EXCEPTION = InstantiationExceptionTarget . class ;

    public static final Class < ? > TARGET_INVOCATION_EXCEPTION = ConstructorTargetInvocationExceptionTarget . class ;

    public static final Class < ? > ILLEGAL_ACCESS_EXCEPTION = IllegalAccessExceptionTarget . class ;

    public static final Class < ? > CLASS_CONSTANT = ConcreteObjectTarget . class ;

    public static final int INTEGER_CONSTANT = 0 ;

    public static final String STRING_CONSTANT = "string constant" ;

    public static < T > T [ ] array ( final Class < T > clazz , final T ... args )
    {
	return args ;
    }
}
