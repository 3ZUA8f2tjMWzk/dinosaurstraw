///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import java . util . Map ;
import javassist . util . proxy . MethodHandler ;
import javassist . util . proxy . ProxyFactory ;
import javassist . util . proxy . ProxyObject ;

abstract class AbstractDinosaurStrawFactory
{
    public final < T > T make ( final Class < ? extends T > superclass )
    {
	final Map < String , Object > parametersMap = emptyMap ( ) ;
	return make ( superclass , parametersMap ) ;
    }

    public final < T > T make ( final Class < ? extends T > superclass , final Map < String , Object > parameterMap )
    {
	final ProxyFactory proxyFactory = proxyFactory ( ) ;
	setSuperclass ( proxyFactory , superclass ) ;
	final Object object = newInstance ( createClass ( proxyFactory ) ) ;
	setHandler ( cast ( proxyObjectClass ( ) , object ) , methodHandler ( parameterMap ) ) ;
	return cast ( superclass , object ) ;
    }

    abstract ProxyFactory proxyFactory ( ) ;

    abstract void setSuperclass ( ProxyFactory proxyFactory , Class < ? > superclass ) ;

    abstract Class < ? > createClass ( ProxyFactory proxyFactory ) ;

    abstract < T > T newInstance ( Class < ? extends T > clazz ) ;

    abstract < T > T cast ( Class < ? extends T > clazz , Object object ) ;

    abstract void setHandler ( ProxyObject proxyObject , MethodHandler methodHandler ) ;

    abstract Class < ? extends ProxyObject > proxyObjectClass ( ) ;

    abstract MethodHandler methodHandler ( Map < String , Object > parameters ) ;

    abstract < K , V > Map < K , V > emptyMap ( ) ;
}
