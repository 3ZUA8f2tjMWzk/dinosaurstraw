///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import com . thoughtworks . paranamer . Paranamer ;
import java . lang . reflect . AccessibleObject ;
import java . util . AbstractMap ;
import java . util . Collection ;
import java . util . List ;
import java . util . Map . Entry ;
import java . util . Set ;

abstract class AbstractParameterMap < V > extends AbstractMap < String , V >
{
    @ Override
	public final Set < Entry < String , V > > entrySet ( )
	{
	    return getSet ( staticInterface ( ) , transform ( staticInterface ( ) , asList ( staticInterface ( ) , lookupParameterNames ( getParanamer ( ) , getMethod ( ) ) ) , getParameterMapFunction ( lookupParameterNames ( getParanamer ( ) , getMethod ( ) ) , getArguments ( ) ) ) ) ;
	}

    abstract V [ ] getArguments ( ) ;

    abstract StaticInterface staticInterface ( ) ;

    abstract < T > List < T > asList ( StaticInterface staticInterface , T [ ] array ) ;

    abstract AccessibleObject getMethod ( ) ;

    abstract Paranamer getParanamer ( ) ;

    abstract String [ ] lookupParameterNames ( Paranamer paranamer , AccessibleObject method ) ;

    abstract Function < ? super String , ? extends Entry < String , V > > getParameterMapFunction ( String [ ] parameterNames , V [ ] arguments ) ;

    abstract < F , T > List < T > transform ( StaticInterface staticInterface , List < F > list , Function < ? super F , ? extends T > function ) ;

    abstract < T > Set < T > getSet ( StaticInterface staticInterface , Collection < T > collection ) ;
}
