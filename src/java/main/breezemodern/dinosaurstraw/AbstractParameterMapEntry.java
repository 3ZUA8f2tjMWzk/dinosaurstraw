///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . BlueAutumn ;
import java . util . List ;
import java . util . Map . Entry ;

abstract class AbstractParameterMapEntry < K , V > implements Entry < K , V >
{
    @ Override
	public final V getValue ( )
	{
	    return cast ( vClass ( ) , get ( staticInterface ( ) , getArguments ( ) , indexOf ( asList ( staticInterface ( ) , getKeys ( ) ) , getKey ( ) ) ) ) ;
	}

    @ Override
	@ BlueAutumn
	public final V setValue ( final V value )
	{
	    throw new UnsupportedOperationException ( ) ;
	}

    abstract K [ ] getKeys ( ) ;

    abstract StaticInterface staticInterface ( ) ;

    abstract < T > List < T > asList ( StaticInterface staticInterface , T [ ] array ) ;

    abstract < T > Integer indexOf ( List < T > list , T item ) ;

    abstract V [ ] getArguments ( ) ;

    abstract Object get ( StaticInterface staticInterface , Object array , Integer index ) ;

    abstract Class < V > vClass ( ) ;

    abstract < V > V cast ( Class < V > clazz , Object object ) ;
}
