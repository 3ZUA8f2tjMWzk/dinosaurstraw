///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . UseArray ;
import breezemodern . easterncrayon . UseClassConstant ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseIntegerConstant ;
import breezemodern . easterncrayon . UseStaticMethod ;
import breezemodern . easterncrayon . UseStringConstant ;
import java . lang . annotation . Annotation ;
import java . lang . reflect . AccessibleObject ;
import java . lang . reflect . Constructor ;
import java . lang . reflect . InvocationTargetException ;
import java . lang . reflect . Method ;
import java . util . Map ;
import javassist . util . proxy . MethodHandler ;

abstract class AbstractDinosaurStrawMethodHandler implements MethodHandler
{
    @ Override
	public final Object invoke ( final Object self , final Method thisMethod , final Method proceed , final Object [ ] args ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
    {
	final Object invoke ;
	if ( null != getAnnotation ( thisMethod , useStringConstantClass ( ) ) )
	    {
		invoke = value ( getAnnotation ( thisMethod , useStringConstantClass ( ) ) ) ;
	    }
	else if ( null != getAnnotation ( thisMethod , useClassConstantClass ( ) ) )
	    {
		invoke = value ( getAnnotation ( thisMethod , useClassConstantClass ( ) ) ) ;
	    }
	else if ( null != getAnnotation ( thisMethod , useIntegerConstantClass ( ) ) )
	    {
		invoke = value ( getAnnotation ( thisMethod , useIntegerConstantClass ( ) ) ) ;
	    }
	else if ( null != getAnnotation ( thisMethod , useConstructorClass ( ) ) )
	    {
		if ( isAbstract ( getModifiers ( getReturnType ( thisMethod ) ) ) )
		    {
			invoke = make ( getDinosaurStrawFactory ( ) , value ( getAnnotation ( thisMethod , useConstructorClass ( ) ) ) , getParameterMap ( args , thisMethod ) ) ;
		    }
		else
		    {
			invoke = newInstance ( util ( ) , getConstructor ( value ( getAnnotation ( thisMethod , useConstructorClass ( ) ) ) , getParameterTypes ( thisMethod ) ) , args ) ;
		    }
	    }
	else if ( null != getAnnotation ( thisMethod , useArrayClass ( ) ) )
	    {
		invoke = args [ zero ( ) ] ;
	    }
	else if ( null != getAnnotation ( thisMethod , useStaticMethodClass ( ) ) )
	    {
		invoke = invoke ( getMethod ( value ( getAnnotation ( thisMethod , useStaticMethodClass ( ) ) ) , getName ( thisMethod ) , getParameterTypes ( thisMethod ) ) , null , args ) ;
	    }
	else if ( null != getAnnotation ( thisMethod , useInstanceMethodClass ( ) ) )
	    {
		invoke = invoke ( getMethod ( getClass ( get ( args , zero ( ) ) ) , getName ( thisMethod ) , copyOfRange ( getParameterTypes ( thisMethod ) , one ( ) , getLength ( args ) ) ) , get ( args , zero ( ) ) , copyOfRange ( args , one ( ) , getLength ( args ) ) ) ;
	    }
	else
	    {
		if ( containsKey ( getParametersMap ( ) , getName ( thisMethod ) ) )
		    {
			invoke = get ( getParametersMap ( ) , getName ( thisMethod ) ) ;
		    }
		else
		    {
			throw runtimeException ( ) ;
		    }
	    }
	return invoke ;
    }

    abstract UtilInterface util ( ) ;

    abstract Integer zero ( ) ;

    abstract < A extends Annotation > A getAnnotation ( Method proceed , Class < A > clazz ) ;

    abstract Class < ? extends UseStringConstant > useStringConstantClass ( ) ;

    abstract String value ( UseStringConstant useStringConstant ) ;

    abstract Class < ? extends UseClassConstant > useClassConstantClass ( ) ;

    abstract Class < ? > value ( UseClassConstant useClassConstant ) ;

    abstract Class < ? extends UseIntegerConstant > useIntegerConstantClass ( ) ;

    abstract Integer value ( UseIntegerConstant useIntegerConstant ) ;

    abstract Class < ? extends UseConstructor > useConstructorClass ( ) ;

    abstract Boolean isAbstract ( Integer modifiers ) ;

    abstract Integer getModifiers ( Class < ? > clazz ) ;

    abstract AbstractDinosaurStrawFactory getDinosaurStrawFactory ( ) ;

    abstract < T > T make ( AbstractDinosaurStrawFactory factory , Class < ? extends T > clazz , Map < String , Object > parametersMap ) ;

    abstract Class < ? > value ( UseConstructor useConstructor ) ;

    abstract Class < ? > getReturnType ( Method method ) ;

    abstract < T > T newInstance ( UtilInterface util , Constructor < T > constructor , Object ... args ) throws IllegalAccessException , InvocationTargetException ;

    abstract < T > Constructor < T > getConstructor ( Class < T > clazz , Class < ? > [ ] parameters ) throws NoSuchMethodException ;

    abstract Map < String , Object > getParameterMap ( Object [ ] args , AccessibleObject method ) ;

    abstract Class < ? extends UseArray > useArrayClass ( ) ;

    abstract Class < ? extends UseStaticMethod > useStaticMethodClass ( ) ;

    abstract Class < ? > value ( UseStaticMethod useStaticMethod ) ;

    abstract Class < ? > [ ] getParameterTypes ( Method method ) ;

    abstract String getName ( Method method ) ;

    abstract Method getMethod ( Class < ? > clazz , String name , Class < ? > [ ] parameterTypes ) throws NoSuchMethodException ;

    abstract Object invoke ( Method method , Object instance , Object [ ] args ) throws IllegalAccessException , InvocationTargetException ;

    abstract Object get ( Object [ ] array , Integer index ) ;

    abstract Integer one ( ) ;

    abstract Integer getLength ( Object [ ] array ) ;

    abstract < T > T [ ] copyOfRange ( T [ ] array , Integer floor , Integer ceiling ) ;

    abstract Class < ? > getClass ( Object object ) ;

    abstract Class < ? extends UseInstanceMethod > useInstanceMethodClass ( ) ;

    abstract Map < String , Object > getParametersMap ( ) ;

    abstract Object get ( Map < String , Object > parametersMap , String key ) ;

    abstract < K > Boolean containsKey ( Map < K , ? > map , K key ) ;

    abstract RuntimeException runtimeException ( ) ;
}
