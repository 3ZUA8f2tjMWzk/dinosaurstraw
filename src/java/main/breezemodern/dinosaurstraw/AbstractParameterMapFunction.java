///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import java . util . Map . Entry ;

abstract class AbstractParameterMapFunction < K , V > implements Function < K , Entry < K , V > >
{
    @ Override
	public final Entry < K , V > apply ( final K input )
	{
	    return getEntry ( input , getKeys ( ) , getArguments ( ) ) ;
	}

    abstract K [ ] getKeys ( ) ;

    abstract V [ ] getArguments ( ) ;

    abstract Entry < K , V > getEntry ( K key , K [ ] keys , V [ ] arguments ) ;
}
