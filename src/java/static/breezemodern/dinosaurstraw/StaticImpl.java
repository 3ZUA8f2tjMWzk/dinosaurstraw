///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import com . google . common . collect . Lists ;
import java . lang . reflect . Array ;
import java . util . Arrays ;
import java . util . Collection ;
import java . util . HashSet ;
import java . util . List ;
import java . util . Set ;

class StaticImpl implements StaticInterface
{
    @ Override
	public final < T > List < T > asList ( final T [ ] array )
    {
	return Arrays . asList ( array ) ;
    }

    @ Override
	public final Object get ( final Object array , final Integer index )
    {
	return Array . get ( array , index ) ;
    }

    @ Override
	public final < T > Set < T > getSet ( final Collection < T > collection )
    {
	return new HashSet < T > ( collection ) ;
    }

    @ Override
	public final < F , T > List < T > transform ( final List < F > input , final Function < ? super F , ? extends T > function )
    {
	return Lists . transform ( input , function ) ;
    }
}
