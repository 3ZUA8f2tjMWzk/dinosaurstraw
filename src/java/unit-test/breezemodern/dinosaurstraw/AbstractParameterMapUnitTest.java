///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import com . thoughtworks . paranamer . Paranamer ;
import java . lang . reflect . AccessibleObject ;
import java . util . List ;
import java . util . Map . Entry ;
import java . util . Set ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class AbstractParameterMapUnitTest
{
    @ Test
	public void test ( )
    {
	final AbstractParameterMap map = mock ( AbstractParameterMap . class ) ;
	final Object [ ] arguments = array ( Object . class ) ;
	final StaticInterface staticInterface = mock ( StaticInterface . class ) ;
	final List < Object > list1 = mock ( List . class ) ;
	final AccessibleObject method = mock ( AccessibleObject . class ) ;
	final Paranamer paranamer = mock ( Paranamer . class ) ;
	final String [ ] parameterNames = array ( String . class ) ;
	final Function < String , Entry < String , Object > > function = mock ( Function . class ) ;
	final List < Entry < String , Object > > list2 = mock ( List . class ) ;
	final Set < Entry < String , Object > > set = mock ( Set . class ) ;
	doReturn ( arguments ) . when ( map ) . getArguments ( ) ;
	doReturn ( staticInterface ) . when ( map ) . staticInterface ( ) ;
	doReturn ( list1 ) . when ( map ) . asList ( staticInterface , arguments ) ;
	doReturn ( method ) . when ( map ) . getMethod ( ) ;
	doReturn ( paranamer ) . when ( map ) . getParanamer ( ) ;
	doReturn ( parameterNames ) . when ( map ) . lookupParameterNames ( paranamer , method ) ;
	doReturn ( function ) . when ( map ) . getParameterMapFunction ( parameterNames , arguments ) ;
	doReturn ( list2 ) . when ( map ) . transform ( staticInterface , list1 , function ) ;
	doReturn ( set ) . when ( map ) . getSet ( staticInterface , list2 ) ;
	assertEquals ( set , map . entrySet ( ) ) ;
    }
}
