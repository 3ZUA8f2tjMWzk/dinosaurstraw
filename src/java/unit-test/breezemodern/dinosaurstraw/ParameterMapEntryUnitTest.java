///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import java . util . List ;
import java . util . Map . Entry ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . INSTANCE ;
import static breezemodern . blueautumn . Targets . INSTANCE_CLASS ;
import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class ParameterMapEntryUnitTest
{
    @ Test
	public void testGetKey ( )
    {
	final String key = mock ( String . class ) ;
	final String [ ] keys = array ( String . class ) ;
	final Object [ ] arguments = array ( Object . class ) ;
	assertEquals ( key , new ParameterMapEntry ( key , keys , arguments ) . getKey ( ) ) ;
    }

    @ Test
	public void testGetKeys ( )
    {
	final String key = mock ( String . class ) ;
	final String [ ] keys = array ( String . class ) ;
	final Object [ ] arguments = array ( Object . class ) ;
	assertEquals ( keys , new ParameterMapEntry ( key , keys , arguments ) . getKeys ( ) ) ;
    }

    @ Test
	public void testStaticInterface ( )
    {
	assertNotNull ( mock ( ParameterMapEntry . class ) . staticInterface ( ) ) ;
    }

    @ Test
	public void testAsList ( )
    {
	final ParameterMapEntry entry = mock ( ParameterMapEntry . class ) ;
	final Object [ ] array = array ( Object . class ) ;
	final StaticInterface st = mock ( StaticInterface . class ) ;
	final List < Object > list = mock ( List . class ) ;
	doReturn ( list ) . when ( st ) . asList ( array ) ;
	assertEquals ( list , entry . asList ( st , array ) ) ;
    }

    @ Test
	public void testIndexOf ( )
    {
	final ParameterMapEntry entry = mock ( ParameterMapEntry . class ) ;
	final List < Object > list = mock ( List . class ) ;
	final Object item = mock ( Object . class ) ;
	final Integer indexOf = mock ( Integer . class ) ;
	doReturn ( indexOf ) . when ( list ) . indexOf ( item ) ;
	assertEquals ( indexOf , entry . indexOf ( list , item ) ) ;
    }

    @ Test
	public void testGetArguments ( )
    {
	final String key = mock ( String . class ) ;
	final String [ ] keys = array ( String . class ) ;
	final Object [ ] arguments = array ( Object . class ) ;
	assertEquals ( arguments , new ParameterMapEntry ( key , keys , arguments ) . getArguments ( ) ) ;
    }

    @ Test
	public void testGet ( )
    {
	final ParameterMapEntry entry = mock ( ParameterMapEntry . class ) ;
	final StaticInterface st = mock ( StaticInterface . class ) ;
	final Object object = mock ( Object . class ) ;
	final Integer index = mock ( Integer . class ) ;
	final Object value = mock ( Object . class ) ;
	doReturn ( value ) . when ( st ) . get ( object , index ) ;
	assertEquals ( value , entry . get ( st , object , index ) ) ;
    }

    @ Test
	public void testCast ( )
    {
	final ParameterMapEntry entry = mock ( ParameterMapEntry . class ) ;
	assertEquals ( INSTANCE , entry . cast ( INSTANCE_CLASS , INSTANCE ) ) ;
    }

    @ Test ( expected = UnsupportedOperationException . class )
	@ AggressiveEagle
	public void bridges ( )
    {
	final String key = mock ( String . class ) ;
	final String [ ] keys = array ( String . class , key ) ;
	final Object [ ] arguments = array ( Object . class , this ) ;
	final Entry entry = new ParameterMapEntry ( key , keys , arguments ) ;
	entry . getKey ( ) ;
	assertEquals ( this , entry . getValue ( ) ) ;
	entry . setValue ( this ) ;
    }
}
