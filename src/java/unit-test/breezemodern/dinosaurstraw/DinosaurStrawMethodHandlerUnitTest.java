///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import breezemodern . easterncrayon . UseArray ;
import breezemodern . easterncrayon . UseClassConstant ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseIntegerConstant ;
import breezemodern . easterncrayon . UseStaticMethod ;
import breezemodern . easterncrayon . UseStringConstant ;
import java . lang . reflect . AccessibleObject ;
import java . lang . reflect . Constructor ;
import java . lang . reflect . InvocationTargetException ;
import java . lang . reflect . Method ;
import java . util . Map ;
import org . junit . Before ;
import org . junit . Test ;
import org . junit . runner . RunWith ;
import org . powermock . core . classloader . annotations . PrepareForTest ;
import org . powermock . modules . junit4 . PowerMockRunner ;

import static breezemodern . blueautumn . Targets . ABSTRACT_OBJECT ;
import static breezemodern . blueautumn . Targets . ANNOTATION_CLASS ;
import static breezemodern . blueautumn . Targets . CONCRETE_OBJECT ;
import static breezemodern . blueautumn . Targets . EMPTY_ARGUMENTS ;
import static breezemodern . blueautumn . Targets . EMPTY_PARAMETERS_MAP ;
import static breezemodern . blueautumn . Targets . EMPTY_PARAMETER_TYPES ;
import static breezemodern . blueautumn . Targets . INSTANCE ;
import static breezemodern . blueautumn . Targets . INSTANCE_CLASS ;
import static breezemodern . blueautumn . Targets . INTERFACE_OBJECT ;
import static breezemodern . blueautumn . Targets . NO_SUCH_METHOD_EXCEPTION ;
import static breezemodern . blueautumn . Targets . OBJECT_METHOD_NAME ;
import static breezemodern . blueautumn . Targets . SINGLE_ARGUMENT ;
import static breezemodern . blueautumn . Targets . SINGLE_ARGUMENTS ;
import static breezemodern . blueautumn . Targets . SINGLE_PARAMETER_TYPES ;
import static breezemodern . blueautumn . Targets . array ;
import static breezemodern . dinosaurstraw . AbstractDinosaurStrawFactoryUnitTest . abstractDinosaurStrawMethodFactory ;
import static java . lang . Integer . valueOf ;
import static java . lang . reflect . Modifier . ABSTRACT ;
import static java . lang . reflect . Modifier . FINAL ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertFalse ;
import static org . junit . Assert . assertNotNull ;
import static org . junit . Assert . assertTrue ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

@ RunWith ( PowerMockRunner . class )
    public final class DinosaurStrawMethodHandlerUnitTest
    {
	private static final Integer ZERO = 0 ;

	private static final Integer ONE = 1 ;

	private Object _instance ;

	private Method _method ;

	private Constructor < ? > _constructor ;

	private AbstractDinosaurStrawMethodHandler _methodHandler ;

	@ Before
	    public void setupMethodHandler ( )
	{
	    _methodHandler = mock ( DinosaurStrawMethodHandler . class ) ;
	}

	@ Before
	    public void setupMethod ( ) throws NoSuchMethodException , InstantiationException , InvocationTargetException , IllegalAccessException
	{
	    _method = CONCRETE_OBJECT . getMethod ( OBJECT_METHOD_NAME , SINGLE_PARAMETER_TYPES ) ;
	    _constructor = CONCRETE_OBJECT . getConstructor ( EMPTY_PARAMETER_TYPES ) ;
	    _instance = _constructor . newInstance ( ) ;
	}

	@ Test
	    public void testConstructor ( )
	{
	    final Map < String , Object > parametersMap = mock ( Map . class ) ;
	    final AbstractDinosaurStrawMethodHandler handler = new DinosaurStrawMethodHandler ( parametersMap ) ;
	    assertEquals ( parametersMap , handler . getParametersMap ( ) ) ;
	}

	@ Test
	    public void testUtil ( )
	{
	    assertNotNull ( _methodHandler . util ( ) ) ;
	}

	@ Test
	    @ PrepareForTest ( Method . class )
	    public void testGetAnnotation ( )
	{
	    assertNotNull ( _methodHandler . getAnnotation ( _method , ANNOTATION_CLASS ) ) ;
	    assertEquals ( _method . getAnnotation ( ANNOTATION_CLASS ) , _methodHandler . getAnnotation ( _method , ANNOTATION_CLASS ) ) ;
	}

	@ Test
	    public void testUseStringConstantClass ( )
	{
	    assertEquals ( UseStringConstant . class , _methodHandler . useStringConstantClass ( ) ) ;
	}

	@ Test
	    public void testValueUseStringConstant ( )
	{
	    final UseStringConstant useStringConstant = mock ( UseStringConstant . class ) ;
	    final String value = mock ( String . class ) ;
	    doReturn ( value ) . when ( useStringConstant ) . value ( ) ;
	    assertEquals ( value , _methodHandler . value ( useStringConstant ) ) ;
	}

	@ Test
	    public void testUseClassConstantClass ( )
	{
	    assertEquals ( UseClassConstant . class , _methodHandler . useClassConstantClass ( ) ) ;
	}

	@ Test
	    public void testValueUseClassConstant ( )
	{
	    final UseClassConstant useClassConstant = mock ( UseClassConstant . class ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    doReturn ( value ) . when ( useClassConstant ) . value ( ) ;
	    assertEquals ( value , _methodHandler . value ( useClassConstant ) ) ;
	}

	@ Test
	    public void testUseIntegerConstantClass ( )
	{
	    assertEquals ( UseIntegerConstant . class , _methodHandler . useIntegerConstantClass ( ) ) ;
	}

	@ Test
	    public void testValueUseIntegerConstant ( )
	{
	    final UseIntegerConstant useIntegerConstant = mock ( UseIntegerConstant . class ) ;
	    final Integer value = mock ( Integer . class ) ;
	    doReturn ( value ) . when ( useIntegerConstant ) . value ( ) ;
	    assertEquals ( value , _methodHandler . value ( useIntegerConstant ) ) ;
	}

	@ Test
	    public void testUseConstructorClass ( )
	{
	    assertEquals ( UseConstructor . class , _methodHandler . useConstructorClass ( ) ) ;
	}

	@ Test
	    public void testGetReturnType ( )
	{
	    assertEquals ( INTERFACE_OBJECT , _methodHandler . getReturnType ( _method ) ) ;
	}

	@ Test
	    public void testGetModifiers ( )
	{
	    assertEquals ( valueOf ( CONCRETE_OBJECT . getModifiers ( ) ) , valueOf ( _methodHandler . getModifiers ( CONCRETE_OBJECT ) ) ) ;
	}

	@ Test
	    public void testIsAbstract ( )
	{
	    assertTrue ( _methodHandler . isAbstract ( ABSTRACT ) ) ;
	    assertFalse ( _methodHandler . isAbstract ( FINAL ) ) ;
	}

	@ Test
	    public void testGetDinosaurStrawFactory ( )
	{
	    assertNotNull ( _methodHandler . getDinosaurStrawFactory ( ) ) ;
	}

	@ Test
	    public void testMake ( )
	{
	    final Object value = mock ( ABSTRACT_OBJECT ) ;
	    assertEquals ( value , _methodHandler . make ( abstractDinosaurStrawMethodFactory ( ABSTRACT_OBJECT , value ) , ABSTRACT_OBJECT , EMPTY_PARAMETERS_MAP ) ) ;
	}

	@ Test
	    public void testGetParameterTypes ( )
	{
	    assertEquals ( SINGLE_PARAMETER_TYPES , _methodHandler . getParameterTypes ( _method ) ) ;
	}

	@ Test
	    public void testValueUseConstructor ( )
	{
	    final UseConstructor useConstructor = mock ( UseConstructor . class ) ;
	    final Class < ? > value = mock ( ABSTRACT_OBJECT ) . getClass ( ) ;
	    doReturn ( value ) . when ( useConstructor ) . value ( ) ;
	    assertEquals ( value , _methodHandler . value ( useConstructor ) ) ;
	}

	@ Test
	    public void testGetConstructor ( ) throws NoSuchMethodException
	{
	    assertEquals ( _constructor , _methodHandler . getConstructor ( CONCRETE_OBJECT , EMPTY_PARAMETER_TYPES ) ) ;
	}

	@ Test ( expected = NoSuchMethodException . class )
	    @ AggressiveEagle
	    public void testGetConstructorNoSuchMethodException ( ) throws NoSuchMethodException
	{
	    _methodHandler . getConstructor ( NO_SUCH_METHOD_EXCEPTION , EMPTY_PARAMETER_TYPES ) ;
	}

	@ Test
	    public void testNewInstance ( ) throws IllegalAccessException , InvocationTargetException
	{
	    final UtilInterface util = mock ( UtilInterface . class ) ;
	    final Object newInstance = mock ( Object . class ) ;
	    doReturn ( _instance ) . when ( util ) . newInstance ( _constructor , EMPTY_ARGUMENTS ) ;
	    assertEquals ( _instance , _methodHandler . newInstance ( util , _constructor , EMPTY_ARGUMENTS ) ) ;
	}

	@ Test
	    public void testZero ( )
	{
	    assertEquals ( ZERO , _methodHandler . zero ( ) ) ;
	}

	@ Test
	    public void testGetParameterMap ( )
	{
	    final Object [ ] args = array ( Object . class ) ;
	    final AccessibleObject thisMethod = mock ( AccessibleObject . class ) ;
	    assertNotNull ( _methodHandler . getParameterMap ( args , thisMethod ) ) ;
	}

	@ Test
	    public void testUseArrayClass ( )
	{
	    assertEquals ( UseArray . class , _methodHandler . useArrayClass ( ) ) ;
	}

	@ Test
	    public void testUseStaticMethodClass ( )
	{
	    assertEquals ( UseStaticMethod . class , _methodHandler . useStaticMethodClass ( ) ) ;
	}

	@ Test
	    public void testValueUseStaticMethod ( )
	{
	    final UseStaticMethod useStaticMethod = mock ( UseStaticMethod . class ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    doReturn ( value ) . when ( useStaticMethod ) . value ( ) ;
	    assertEquals ( value , _methodHandler . value ( useStaticMethod ) ) ;
	}

	@ Test
	    @ PrepareForTest ( { String . class , Method . class , } )
	    public void testGetName ( )
	{
	    assertEquals ( OBJECT_METHOD_NAME , _methodHandler . getName ( _method ) ) ;
	}

	@ Test
	    public void testGetMethod ( ) throws NoSuchMethodException
	{
	    assertEquals ( _method , _methodHandler . getMethod ( CONCRETE_OBJECT , OBJECT_METHOD_NAME , SINGLE_PARAMETER_TYPES ) ) ;
	}

	@ Test
	    @ PrepareForTest ( Method . class )
	    public void testInvoke ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    assertEquals ( SINGLE_ARGUMENT , _methodHandler . invoke ( _method , _instance , SINGLE_ARGUMENTS ) ) ;
	}

	@ Test
	    public void testGet ( )
	{
	    assertEquals ( SINGLE_ARGUMENT , _methodHandler . get ( SINGLE_ARGUMENTS , ZERO ) ) ;
	}

	@ Test
	    public void testOne ( )
	{
	    assertEquals ( ONE , _methodHandler . one ( ) ) ;
	}

	@ Test
	    public void testGetLength ( )
	{
	    assertEquals ( ONE , _methodHandler . getLength ( SINGLE_ARGUMENTS ) ) ;
	}

	@ Test
	    public void testCopyOfRange ( )
	{
	    assertEquals ( SINGLE_ARGUMENTS , _methodHandler . copyOfRange ( SINGLE_ARGUMENTS , ZERO , ONE ) ) ;
	}

	@ Test
	    public void testGetClass ( )
	{
	    assertEquals ( INSTANCE_CLASS , _methodHandler . getClass ( INSTANCE ) ) ;
	}

	@ Test
	    public void testUseInstanceMethodClass ( )
	{
	    assertEquals ( UseInstanceMethod . class , _methodHandler . useInstanceMethodClass ( ) ) ;
	}

	@ Test
	    public void testGetMap ( )
	{
	    final Map < String , Object > map = mock ( Map . class ) ;
	    final String key = mock ( String . class ) ;
	    final Object value = mock ( Object . class ) ;
	    doReturn ( value ) . when ( map ) . get ( key ) ;
	    assertEquals ( value , _methodHandler . get ( map , key ) ) ;
	}

	@ Test
	    public void testContainsKey ( )
	{
	    final Map < String , Object > map = mock ( Map . class ) ;
	    final String key = mock ( String . class ) ;
	    final Boolean containsKey = mock ( Boolean . class ) ;
	    doReturn ( containsKey ) . when ( map ) . containsKey ( key ) ;
	    assertEquals ( containsKey , _methodHandler . containsKey ( map , key ) ) ;
	}

	@ Test
	    public void testRuntimeException ( )
	{
	    assertNotNull ( _methodHandler . runtimeException ( ) ) ;
	}
   }
