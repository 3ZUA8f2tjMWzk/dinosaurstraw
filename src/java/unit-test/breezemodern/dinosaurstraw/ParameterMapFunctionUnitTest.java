///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class ParameterMapFunctionUnitTest
    {
	@ Test
	    public void constructor ( )
	{
	    final String [ ] keys = array ( String . class ) ;
	    final Object [ ] arguments = array ( Object . class ) ;
	    final ParameterMapFunction function = new ParameterMapFunction ( keys , arguments ) ;
	    assertEquals ( keys , function . getKeys ( ) ) ;
	    assertEquals ( arguments , function . getArguments ( ) ) ;
	}

	@ Test
	    public void testGetEntry ( )
	{
	    final ParameterMapFunction function = mock ( ParameterMapFunction . class ) ;
	    final String key = mock ( String . class ) ;
	    final String [ ] keys = array ( String . class ) ;
	    final Object [ ] arguments = array ( Object . class ) ;
	    assertNotNull ( function . getEntry ( key , keys , arguments ) ) ;
	}

	@ Test
	    public void bridges ( )
	{
	    final String [ ] keys = array ( String . class ) ;
	    final Object [ ] arguments = array ( Object . class ) ;
	    final Function function = new ParameterMapFunction ( keys , arguments ) ;
	    final String key = mock ( String . class ) ;
	    function . apply ( key ) ;
	}
    }
