///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import breezemodern . easterncrayon . BlueAutumn ;
import java . util . List ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class AbstractParameterMapEntryUnitTest
    {
	@ Test
	    public void testGetValue ( )
	{
	    final AbstractParameterMapEntry entry = mock ( AbstractParameterMapEntry . class ) ;
	    final Object [ ] args = array ( Object . class ) ;
	    final String key = mock ( String . class ) ;
	    final String [ ] keys = array ( String . class ) ;
	    final StaticInterface st = mock ( StaticInterface . class ) ;
	    final List < String > list = mock ( List . class ) ;
	    final Integer index = mock ( Integer . class ) ;
	    final Object valueObject = mock ( Object . class ) ;
	    final Class < ? > vClass = valueObject . getClass ( ) ;
	    final Object value = mock ( Object . class ) ;
	    doReturn ( args ) . when ( entry ) . getArguments ( ) ;
	    doReturn ( key ) . when ( entry ) . getKey ( ) ;
	    doReturn ( keys ) . when ( entry ) . getKeys ( ) ;
	    doReturn ( st ) . when ( entry ) . staticInterface ( ) ;
	    doReturn ( list ) . when ( entry ) . asList ( st , keys ) ;
	    doReturn ( index ) . when ( entry ) . indexOf ( list , key ) ;
	    doReturn ( valueObject ) . when ( entry ) . get ( st , args , index ) ;
	    doReturn ( vClass ) . when ( entry ) . vClass ( ) ;
	    doReturn ( value ) . when ( entry ) . cast ( vClass , valueObject ) ;
	    assertEquals ( value , entry . getValue ( ) ) ;
	}

	@ Test ( expected = UnsupportedOperationException . class )
	    @ AggressiveEagle
	    @ BlueAutumn
	    public void testSetValue ( )
	{
	    final AbstractParameterMapEntry entry = mock ( AbstractParameterMapEntry . class ) ;
	    final Object value = mock ( Object . class ) ;
	    entry . setValue ( value ) ;
	}
    }
