///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import java . util . Map . Entry ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class AbstractParameterMapFunctionUnitTest
    {
	@ Test
	    public void testApply ( )
	{
	    final AbstractParameterMapFunction < String , Object > function = mock ( AbstractParameterMapFunction . class ) ;
	    final String key = mock ( String . class ) ;
	    final String [ ] keys = array ( String . class ) ;
	    final Object [ ] arguments = array ( Object . class ) ;
	    final Entry < String , Object > entry = mock ( Entry . class ) ;
	    doReturn ( keys ) . when ( function ) . getKeys ( ) ;
	    doReturn ( arguments ) . when ( function ) . getArguments ( ) ;
	    doReturn ( entry ) . when ( function ) . getEntry ( key , keys , arguments ) ;
	    assertEquals ( entry , function . apply ( key ) ) ;
	}
    }
