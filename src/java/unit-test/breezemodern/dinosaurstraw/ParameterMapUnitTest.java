///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import com . google . common . base . Function ;
import com . thoughtworks . paranamer . Paranamer ;
import java . lang . reflect . AccessibleObject ;
import java . util . Collection ;
import java . util . List ;
import java . util . Set ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class ParameterMapUnitTest
{
    @ Test
	public void constructor ( )
    {
	final Object [ ] arguments = array ( Object . class ) ;
	final AccessibleObject method = mock ( AccessibleObject . class ) ;
	final ParameterMap map = new ParameterMap ( arguments , method ) ;
	assertEquals ( arguments , map . getArguments ( ) ) ;
	assertEquals ( method , map . getMethod ( ) ) ;
    }

    @ Test
	public void testStaticInterface ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	assertNotNull ( map . staticInterface ( ) ) ;
    }

    @ Test
	public void testAsList ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	final StaticInterface staticInterface = mock ( StaticInterface . class ) ;
	final Object [ ] array = array ( Object . class ) ;
	final List < Object > list = mock ( List . class ) ;
	doReturn ( list ) . when ( staticInterface ) . asList ( array ) ;
	assertEquals ( list , map . asList ( staticInterface , array ) ) ;
    }

    @ Test
	public void testGetParanamer ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	assertNotNull ( map . getParanamer ( ) ) ;
    }

    @ Test
	public void testLookupParameterNames ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	final Paranamer paranamer = mock ( Paranamer . class ) ;
	final AccessibleObject method = mock ( AccessibleObject . class ) ;
	final String [ ] parameterNames = array ( String . class ) ;
	doReturn ( parameterNames ) . when ( paranamer ) . lookupParameterNames ( method ) ;
	assertEquals ( parameterNames , map . lookupParameterNames ( paranamer , method ) ) ;
    }

    @ Test
	public void testGetParameterMapFunction ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	final String [ ] parameterNames = array ( String . class ) ;
	final Object [ ] arguments = array ( Object . class ) ;
	assertNotNull ( map . getParameterMapFunction ( parameterNames , arguments ) ) ;
    }

    @ Test
	public void testTransform ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	final StaticInterface staticInterface = mock ( StaticInterface . class ) ;
	final List < Object > input = mock ( List . class ) ;
	final Function < Object , Object > function = mock ( Function . class ) ;
	final List < Object > output = mock ( List . class ) ;
	doReturn ( output ) . when ( staticInterface ) . transform ( input , function ) ;
	assertEquals ( output , map . transform ( staticInterface , input , function ) ) ;
    }

    @ Test
	public void testGetSet ( )
    {
	final ParameterMap map = mock ( ParameterMap . class ) ;
	final StaticInterface staticInterface = mock ( StaticInterface . class ) ;
	final Collection < Object > collection = mock ( Collection . class ) ;
	final Set < Object > set = mock ( Set . class ) ;
	doReturn ( set ) . when ( staticInterface ) . getSet ( collection ) ;
	assertEquals ( set , map . getSet ( staticInterface , collection ) ) ;
    }
}
