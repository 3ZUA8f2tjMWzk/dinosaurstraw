///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import breezemodern . easterncrayon . UseArray ;
import breezemodern . easterncrayon . UseClassConstant ;
import breezemodern . easterncrayon . UseConstructor ;
import breezemodern . easterncrayon . UseInstanceMethod ;
import breezemodern . easterncrayon . UseIntegerConstant ;
import breezemodern . easterncrayon . UseParameter ;
import breezemodern . easterncrayon . UseStaticMethod ;
import breezemodern . easterncrayon . UseStringConstant ;
import java . lang . reflect . Constructor ;
import java . lang . reflect . InvocationTargetException ;
import java . lang . reflect . Method ;
import java . util . Map ;
import org . junit . Before ;
import org . junit . Test ;
import org . junit . runner . RunWith ;
import org . powermock . core . classloader . annotations . PrepareForTest ;
import org . powermock . modules . junit4 . PowerMockRunner ;

import static org . junit . Assert . assertEquals ;
import static org . mockito . Mockito . doReturn ;
import static org . powermock . api . mockito . PowerMockito . mock ;

@ RunWith ( PowerMockRunner . class )
    public final class AbstractDinosaurStrawMethodHandlerUnitTest
    {
	private AbstractDinosaurStrawMethodHandler _methodHandler ;

	private Object _self ;

	private Method _thisMethod ;

	private Method _proceed ;

	private Object [ ] _args ;

	private < T > T [ ] array ( final Class < T > clazz , final T ... elements )
	{
	    return elements ;
	}

	@ Before
	    @ PrepareForTest ( Method . class )
	    public void setupMethod ( )
	{
	    _methodHandler = mock ( AbstractDinosaurStrawMethodHandler . class ) ;
	    _self = mock ( Method . class ) ;
	    _thisMethod = mock ( Method . class ) ;
	    _proceed = mock ( Method . class ) ;
	    _args = array ( Object . class , mock ( Object . class ) ) ;
	}

	private void areEquals ( final Object expected ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    assertEquals ( expected , _methodHandler . invoke ( _self , _thisMethod , _proceed , _args ) ) ;
	}

	@ Test
	    @ PrepareForTest ( { Method . class , String . class , } )
	    public void testStringConstant ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseStringConstant useStringConstant = mock ( UseStringConstant . class ) ;
	    final Class < ? extends UseStringConstant > useStringConstantClass = useStringConstant . getClass ( ) ;
	    final String value = mock ( String . class ) ;
	    doReturn ( useStringConstantClass ) . when ( _methodHandler ) . useStringConstantClass ( ) ;
	    doReturn ( useStringConstant ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useStringConstantClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useStringConstant ) ;
	    areEquals ( value ) ;
	}

	@ Test
	    @ PrepareForTest ( Method . class )
	    public void testClassConstant ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseClassConstant useClassConstant = mock ( UseClassConstant . class ) ;
	    final Class < ? extends UseClassConstant > useClassConstantClass = useClassConstant . getClass ( ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    doReturn ( useClassConstantClass ) . when ( _methodHandler ) . useClassConstantClass ( ) ;
	    doReturn ( useClassConstant ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useClassConstantClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useClassConstant ) ;
	    areEquals ( value ) ;
	}

	@ Test
	    @ PrepareForTest ( { Method . class , Integer . class , } )
	    public void testIntegerConstant ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseIntegerConstant useIntegerConstant = mock ( UseIntegerConstant . class ) ;
	    final Class < ? extends UseIntegerConstant > useIntegerConstantClass = useIntegerConstant . getClass ( ) ;
	    final Integer value = mock ( Integer . class ) ;
	    doReturn ( useIntegerConstantClass ) . when ( _methodHandler ) . useIntegerConstantClass ( ) ;
	    doReturn ( useIntegerConstant ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useIntegerConstantClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useIntegerConstant ) ;
	    areEquals ( value ) ;
	}

	@ Test
	    @ PrepareForTest ( Method . class )
	    public void testConstructorAbstract ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseConstructor useConstructor = mock ( UseConstructor . class ) ;
	    final Class < ? extends UseConstructor > useConstructorClass = useConstructor . getClass ( ) ;
	    final Boolean isAbstract = true ;
	    final Integer modifiers = mock ( Integer . class ) ;
	    final AbstractDinosaurStrawFactory dinosaurStrawFactory = mock ( AbstractDinosaurStrawFactory . class ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    final Map < String , Object > parameterMap = mock ( Map . class ) ;
	    final Object newInstance = mock ( Object . class ) ;
	    doReturn ( useConstructorClass ) . when ( _methodHandler ) . useConstructorClass ( ) ;
	    doReturn ( useConstructor ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useConstructorClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useConstructor ) ;
	    doReturn ( modifiers ) . when ( _methodHandler ) . getModifiers ( value ) ;
	    doReturn ( isAbstract ) . when ( _methodHandler ) . isAbstract ( modifiers ) ;
	    doReturn ( dinosaurStrawFactory ) . when ( _methodHandler ) . getDinosaurStrawFactory ( ) ;
	    doReturn ( parameterMap ) . when ( _methodHandler ) . getParameterMap ( _args , _thisMethod ) ;
	    doReturn ( newInstance ) . when ( _methodHandler ) . make ( dinosaurStrawFactory , value , parameterMap ) ;
	    areEquals ( newInstance ) ;
	}

	@ Test
	    @ PrepareForTest ( Method . class )
	    public void testConstructorConcrete ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseConstructor useConstructor = mock ( UseConstructor . class ) ;
	    final Class < ? extends UseConstructor > useConstructorClass = useConstructor . getClass ( ) ;
	    final Boolean isAbstract = false ;
	    final Integer modifiers = mock ( Integer . class ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    final Class < ? > [ ] parameterTypes = array ( Class . class ) ;
	    final UtilInterface util = mock ( UtilInterface . class ) ;
	    final Constructor < ? > constructor = value . getConstructor ( ) ;
	    final Object newInstance = mock ( Object . class ) ;
	    doReturn ( useConstructorClass ) . when ( _methodHandler ) . useConstructorClass ( ) ;
	    doReturn ( useConstructor ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useConstructorClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useConstructor ) ;
	    doReturn ( modifiers ) . when ( _methodHandler ) . getModifiers ( value ) ;
	    doReturn ( isAbstract ) . when ( _methodHandler ) . isAbstract ( modifiers ) ;
	    doReturn ( parameterTypes ) . when ( _methodHandler ) . getParameterTypes ( _thisMethod ) ;
	    doReturn ( constructor ) . when ( _methodHandler ) . getConstructor ( value , parameterTypes ) ;
	    doReturn ( util ) . when ( _methodHandler ) . util ( ) ;
	    doReturn ( newInstance ) . when ( _methodHandler ) . newInstance ( util , constructor , _args ) ;
	    areEquals ( newInstance ) ;
	}

	@ Test
	    @ PrepareForTest ( { Method . class , Integer . class , } )
	    public void testArray ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseArray useArray = mock ( UseArray . class ) ;
	    final Class < ? extends UseArray > useArrayClass = useArray . getClass ( ) ;
	    final Object expected = mock ( Object . class ) ;
	    _args = array ( Object . class , expected ) ;
	    doReturn ( useArrayClass ) . when ( _methodHandler ) . useArrayClass ( ) ;
	    doReturn ( useArray ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useArrayClass ) ;
	    areEquals ( expected ) ;
	}

	@ Test
	    @ PrepareForTest ( { Method . class , Integer . class , String . class , } )
	    public void testStaticMethod ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseStaticMethod useStaticMethod = mock ( UseStaticMethod . class ) ;
	    final Class < ? extends UseStaticMethod > useStaticMethodClass = useStaticMethod . getClass ( ) ;
	    final Class < ? > value = mock ( Object . class ) . getClass ( ) ;
	    final String thisMethodName = mock ( String . class ) ;
	    final Class < ? > [ ] thisMethodParameterTypes = array ( Class . class ) ;
	    final Method method = mock ( Method . class ) ;
	    final Object expected = mock ( Object . class ) ;
	    doReturn ( useStaticMethodClass ) . when ( _methodHandler ) . useStaticMethodClass ( ) ;
	    doReturn ( useStaticMethod ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useStaticMethodClass ) ;
	    doReturn ( value ) . when ( _methodHandler ) . value ( useStaticMethod ) ;
	    doReturn ( thisMethodName ) . when ( _methodHandler ) . getName ( _thisMethod ) ;
	    doReturn ( thisMethodParameterTypes ) . when ( _methodHandler ) . getParameterTypes ( _thisMethod ) ;
	    doReturn ( method ) . when ( _methodHandler ) . getMethod ( value , thisMethodName , thisMethodParameterTypes ) ;
	    doReturn ( expected ) . when ( _methodHandler ) . invoke ( method , null , _args ) ;
	    areEquals ( expected ) ;
	}

	@ Test
	    public void testInstanceMethod ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseInstanceMethod useInstanceMethod = mock ( UseInstanceMethod . class ) ;
	    final Class < ? > [ ] methodParameterTypes = array ( Class . class ) ;
	    final Integer zero = mock ( Integer . class ) ;
	    final Object instanceClassObject = mock ( Object . class ) ;
	    final Class < ? extends Class > classClass = Class . class ;
	    final Class < ? > instanceClass = mock ( Object . class ) . getClass ( ) ;
	    final String name = mock ( String . class ) ;
	    final Class < ? > [ ] thisMethodParameterTypes = array ( Class . class ) ;
	    final Integer one = mock ( Integer . class ) ;
	    final Integer n = mock ( Integer . class ) ;
	    final Method method = mock ( Method . class ) ;
	    final Object [ ] methodArgs = array ( Object . class , mock ( Object . class ) ) ;
	    final Object instance = mock ( Object . class ) ;
	    final Object invoke = mock ( Object . class ) ;
	    doReturn ( useInstanceMethod . getClass ( ) ) . when ( _methodHandler ) . useInstanceMethodClass ( ) ;
	    doReturn ( useInstanceMethod ) . when ( _methodHandler ) . getAnnotation ( _thisMethod , useInstanceMethod . getClass ( ) ) ;
	    doReturn ( thisMethodParameterTypes ) . when ( _methodHandler ) . getParameterTypes ( _thisMethod ) ;
	    doReturn ( zero ) . when ( _methodHandler ) . zero ( ) ;
	    doReturn ( instanceClassObject ) . when ( _methodHandler ) . get ( thisMethodParameterTypes , zero ) ;
	    doReturn ( instanceClass ) . when ( _methodHandler ) . getClass ( instance ) ;
	    doReturn ( name ) . when ( _methodHandler ) . getName ( _thisMethod ) ;
	    doReturn ( one ) . when ( _methodHandler ) . one ( ) ;
	    doReturn ( n ) . when ( _methodHandler ) . getLength ( _args ) ;
	    doReturn ( methodParameterTypes ) . when ( _methodHandler ) . copyOfRange ( thisMethodParameterTypes , one , n ) ;
	    doReturn ( method ) . when ( _methodHandler ) . getMethod ( instanceClass , name , methodParameterTypes ) ;
	    doReturn ( instance ) . when ( _methodHandler ) . get ( _args , zero ) ;
	    doReturn ( methodArgs ) . when ( _methodHandler ) . copyOfRange ( _args , one , n ) ;
	    doReturn ( invoke ) . when ( _methodHandler ) . invoke ( method , instance , methodArgs ) ;
	    areEquals ( invoke ) ;
	}

	@ Test
	    public void testParameterTrue ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseParameter useParameter = mock ( UseParameter . class ) ;
	    final String name = mock ( String . class ) ;
	    final Map < String , Object > parametersMap = mock ( Map . class ) ;
	    final Boolean containsKey = true ;
	    final Object parameter = mock ( Object . class ) ;
	    doReturn ( name ) . when ( _methodHandler ) . getName ( _thisMethod ) ;
	    doReturn ( parametersMap ) . when ( _methodHandler ) . getParametersMap ( ) ;
	    doReturn ( containsKey ) . when ( _methodHandler ) . containsKey ( parametersMap , name ) ;
	    doReturn ( parameter ) . when ( _methodHandler ) . get ( parametersMap , name ) ;
	    areEquals ( parameter ) ;
	}

	@ Test ( expected = RuntimeException . class )
	    @ AggressiveEagle
	    public void testParameterFalse ( ) throws NoSuchMethodException , IllegalAccessException , InvocationTargetException
	{
	    final UseParameter useParameter = mock ( UseParameter . class ) ;
	    final String name = mock ( String . class ) ;
	    final Map < String , Object > parametersMap = mock ( Map . class ) ;
	    final Boolean containsKey = false ;
	    final RuntimeException runtimeException = mock ( RuntimeException . class ) ;
	    doReturn ( name ) . when ( _methodHandler ) . getName ( _thisMethod ) ;
	    doReturn ( parametersMap ) . when ( _methodHandler ) . getParametersMap ( ) ;
	    doReturn ( containsKey ) . when ( _methodHandler ) . containsKey ( parametersMap , name ) ;
	    doReturn ( runtimeException ) . when ( _methodHandler ) . runtimeException ( ) ;
	    _methodHandler . invoke ( _self , _thisMethod , _proceed , _args ) ;
	}
    }
