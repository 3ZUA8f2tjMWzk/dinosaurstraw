///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import java . util . Map ;
import javassist . util . proxy . MethodHandler ;
import javassist . util . proxy . ProxyFactory ;
import javassist . util . proxy . ProxyObject ;
import org . junit . Before ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . ABSTRACT_OBJECT ;
import static breezemodern . blueautumn . Targets . CONCRETE_OBJECT ;
import static breezemodern . blueautumn . Targets . ILLEGAL_ACCESS_EXCEPTION ;
import static breezemodern . blueautumn . Targets . INSTANTIATION_EXCEPTION ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . mockito . Mockito . mock ;
import static org . mockito . Mockito . verify ;

public final class DinosaurStrawFactoryUnitTest
    {
	private DinosaurStrawFactory _factory ;

	@ Before
	    public void setup ( )
	{
	    _factory = mock ( DinosaurStrawFactory . class ) ;
	}

	@ Test
	    public void constructor ( )
	{
	    assertNotNull ( new DinosaurStrawFactory ( ) ) ;
	}

	@ Test
	    public void testProxyFactory ( )
	{
	    assertNotNull ( _factory . proxyFactory ( ) ) ;
	}

	@ Test
	    public void testSetSuperclass ( )
	{
	    final ProxyFactory proxyFactory = mock ( ProxyFactory . class ) ;
	    final Class < ? > superclass = mock ( Object . class ) . getClass ( ) ;
	    _factory . setSuperclass ( proxyFactory , superclass ) ;
	    verify ( proxyFactory ) . setSuperclass ( superclass ) ;
	}

	@ Test
	    public void testCreateClass ( )
	{
	    final ProxyFactory proxyFactory = mock ( ProxyFactory . class ) ;
	    _factory . createClass ( proxyFactory ) ;
	    verify ( proxyFactory ) . createClass ( ) ;
	}

	@ Test
	    public void newInstance ( ) throws InstantiationException , IllegalAccessException
	{
	    assertNotNull ( _factory . newInstance ( CONCRETE_OBJECT ) ) ;
	}

	@ Test ( expected = RuntimeException . class )
	    @ AggressiveEagle
	    public void testNewInstanceInstantiationException ( )
	{
	    _factory . newInstance ( INSTANTIATION_EXCEPTION ) ;
	}

	@ Test ( expected = RuntimeException . class )
	    @ AggressiveEagle
	    public void testNewInstanceIllegalAccessException ( )
	{
	    _factory . newInstance ( ILLEGAL_ACCESS_EXCEPTION ) ;
	}

	@ Test
	    public void testCast ( )
	{
	    final Object object = mock ( ABSTRACT_OBJECT ) ;
	    assertEquals ( object , _factory . cast ( ABSTRACT_OBJECT , object ) ) ;
	}

	@ Test
	    public void testSetHandler ( )
	{
	    final ProxyObject proxyObject = mock ( ProxyObject . class ) ;
	    final MethodHandler handler = mock ( MethodHandler . class ) ;
	    _factory . setHandler ( proxyObject , handler ) ;
	    verify ( proxyObject ) . setHandler ( handler ) ;
	}

	@ Test
	    public void testProxyObjectClass ( )
	{
	    assertEquals ( ProxyObject . class , _factory . proxyObjectClass ( ) ) ;
	}

	@ Test
	    public void testMethodHandler ( )
	{
	    final Map < String , Object > parametersMap = mock ( Map . class ) ;
	    assertNotNull ( _factory . methodHandler ( parametersMap ) ) ;
	}

	@ Test
	    public void testEmptyMap ( )
	{
	    assertNotNull ( _factory . emptyMap ( ) ) ;
	}
    }
