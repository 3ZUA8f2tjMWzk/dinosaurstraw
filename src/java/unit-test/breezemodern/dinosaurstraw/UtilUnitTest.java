///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import breezemodern . easterncrayon . AggressiveEagle ;
import java . lang . reflect . Constructor ;
import java . lang . reflect . InvocationTargetException ;
import org . junit . Before ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . CONCRETE_OBJECT ;
import static breezemodern . blueautumn . Targets . ILLEGAL_ACCESS_EXCEPTION ;
import static breezemodern . blueautumn . Targets . INSTANTIATION_EXCEPTION ;
import static breezemodern . blueautumn . Targets . TARGET_INVOCATION_EXCEPTION ;
import static breezemodern . blueautumn . Targets . array ;
import static org . junit . Assert . assertNotEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . powermock . api . mockito . PowerMockito . mock ;

public final class UtilUnitTest
    {
	private UtilInterface _util ;

	private Constructor < ? > _object ;

	private Constructor < ? > _instantiationException ;

	private Constructor < ? > _illegalAccessException ;

	private Constructor < ? > _targetInvocationException ;

	@ Before
	    public void setup ( ) throws NoSuchMethodException
	{
	    _util = mock ( UtilImpl . class ) ;
	    _object = CONCRETE_OBJECT . getConstructor ( array ( Class . class ) ) ;
	    _instantiationException = INSTANTIATION_EXCEPTION . getConstructor ( array ( Class . class ) ) ;
	    _illegalAccessException = ILLEGAL_ACCESS_EXCEPTION . getConstructor ( array ( Class . class ) ) ;
	    _targetInvocationException = TARGET_INVOCATION_EXCEPTION . getConstructor ( array ( Class . class ) ) ;
	}

	@ Test
	    public void constructor ( )
	{
	    assertNotNull ( new UtilImpl ( ) ) ;
	}

	@ Test
	    public void testNewInstance ( ) throws IllegalAccessException , InvocationTargetException
	{
	    assertNotNull ( _util . newInstance ( _object , array ( Object . class ) ) ) ;
	    assertNotEquals ( _util . newInstance ( _object , array ( Object . class ) ) , _util . newInstance ( _object , array ( Object . class ) ) ) ;
	}

	@ Test ( expected = RuntimeException . class )
	    @ AggressiveEagle
	    public void testNewInstanceInstantiationException ( ) throws  IllegalAccessException , InvocationTargetException
	{
	    _util . newInstance ( _instantiationException , array ( Object . class ) ) ;
	}

	@ Test ( expected = IllegalAccessException . class )
	    @ AggressiveEagle
	    public void testNewInstanceIllegalAccessException ( ) throws IllegalAccessException , InvocationTargetException
	{
	    _util . newInstance ( _illegalAccessException , array ( Object . class ) ) ;
	}

	@ Test ( expected = InvocationTargetException . class )
	    @ AggressiveEagle
	    public void testNewInstanceInvocationTargetException ( ) throws IllegalAccessException , InvocationTargetException
	{
	    _util . newInstance ( _targetInvocationException , array ( Object . class ) ) ;
	}
    }
