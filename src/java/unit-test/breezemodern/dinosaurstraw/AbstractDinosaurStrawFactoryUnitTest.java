///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . dinosaurstraw ;

import java . util . Map ;
import javassist . util . proxy . ProxyFactory ;
import javassist . util . proxy . ProxyObject ;
import org . junit . Test ;

import static org . junit . Assert . assertEquals ;
import static org . mockito . Mockito . doReturn ;
import static org . mockito . Mockito . mock ;

public final class AbstractDinosaurStrawFactoryUnitTest
    {
	static AbstractDinosaurStrawFactory abstractDinosaurStrawMethodFactory ( final Class < ? > superclass , final Object value )
	{
	    final AbstractDinosaurStrawFactory factory = mock ( AbstractDinosaurStrawFactory . class ) ;
	    final ProxyFactory proxyFactory = mock ( ProxyFactory . class ) ;
	    final Object object = mock ( Object . class ) ;
	    final Class < ? > createClass = mock ( Object . class ) . getClass ( ) ;
	    final ProxyObject proxyObject = mock ( ProxyObject . class ) ;
	    doReturn ( proxyFactory ) . when ( factory ) . proxyFactory ( ) ;
	    doReturn ( createClass ) . when ( factory ) . createClass ( proxyFactory ) ;
	    doReturn ( object ) . when ( factory ) . newInstance ( createClass ) ;
	    doReturn ( value ) . when ( factory ) . cast ( superclass , object ) ;
	    return factory ;
	}

	@ Test
	    public void test ( )
	{
	    final AbstractDinosaurStrawFactory factory = mock ( AbstractDinosaurStrawFactory . class ) ;
	    final Class < ? > superclass = mock ( Object . class ) . getClass ( ) ;
	    final ProxyFactory proxyFactory = mock ( ProxyFactory . class ) ;
	    final Class < ? > clazz = mock ( Object . class ) . getClass ( ) ;
	    final Object object = mock ( Object . class ) ;
	    final Class < ? extends ProxyObject > proxyObjectClass = mock ( ProxyObject . class ) . getClass ( ) ;
	    final ProxyObject proxyObject = mock ( ProxyObject . class ) ;
	    final Map < String , Object > parameterMap = mock ( Map . class ) ;
	    final Object cast = mock ( Object . class ) ;
	    doReturn ( proxyFactory ) . when ( factory ) . proxyFactory ( ) ;
	    doReturn ( clazz ) . when ( factory ) . createClass ( proxyFactory ) ;
	    doReturn ( object ) . when ( factory ) . newInstance ( clazz ) ;
	    doReturn ( cast ) . when ( factory ) . cast ( superclass , object ) ;
	    doReturn ( parameterMap ) . when ( factory ) . emptyMap ( ) ;
	    assertEquals ( cast , factory . make ( superclass ) ) ;
	}
    }
