///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . dinosaurstraw . DinosaurStrawFactory ;
import breezemodern . easterncrayon . BlueAutumn ;
import java . io . IOException ;
import java . lang . reflect . InvocationTargetException ;
import org . junit . Before ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . ANNOTATED_OBJECT ;

public final class InvocationTargetExceptionFunctionalTest
{
    private AnnotatedObjectTarget _target ;

    @ Before
	public void setUp ( )
    {
	_target = new DinosaurStrawFactory ( ) . make ( ANNOTATED_OBJECT ) ;
    }

    @ Test ( expected = InvocationTargetException . class )
	@ BlueAutumn
	public void testLegalException ( ) throws IOException
    {
	_target . legalException ( _target . targetInvocationExceptionTarget ( ) ) ;
    }

    @ Test ( expected = InvocationTargetException . class )
	@ BlueAutumn
	public void testIllegalException ( )
    {
	_target . illegalException ( _target . targetInvocationExceptionTarget ( ) ) ;
    }
}
