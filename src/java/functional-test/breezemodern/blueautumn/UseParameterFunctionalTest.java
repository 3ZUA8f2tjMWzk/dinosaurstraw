///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . dinosaurstraw . DinosaurStrawFactory ;
import breezemodern . easterncrayon . BlueAutumn ;
import java . util . HashMap ;
import java . util . Map ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . ANNOTATED_OBJECT ;
import static breezemodern . blueautumn . Targets . INSTANCE ;
import static breezemodern . blueautumn . Targets . PARAMETERIZED_OBJECT ;
import static breezemodern . blueautumn . Targets . SINGLE_ARGUMENT ;
import static org . junit . Assert . assertEquals ;

public final class UseParameterFunctionalTest
{
    static void test ( final AnnotatedObjectTarget target )
    {
	org . junit . Assert . assertNotNull ( target . parameterizedObject ( INSTANCE ) ) ;
	assertEquals ( INSTANCE , target . parameterizedObject ( INSTANCE ) . parameter ( ) ) ;
    }

    @ Test
	public void test ( )
    {
	final AnnotatedObjectTarget target = new DinosaurStrawFactory ( ) . make ( ANNOTATED_OBJECT ) ;
	test ( target . annotatedObject ( ) ) ;
    }

    @ Test
	public void testConstructor ( )
    {
	final Map < String , Object > parameters = new HashMap < String , Object > ( ) ;
	parameters . put ( "parameter" , SINGLE_ARGUMENT ) ;
	assertEquals ( SINGLE_ARGUMENT , new DinosaurStrawFactory ( ) . make ( PARAMETERIZED_OBJECT , parameters ) . parameter ( ) ) ;
    }

    @ Test ( expected = RuntimeException . class )
	@ BlueAutumn
	public void testParameterMismatch ( )
    {
	new DinosaurStrawFactory ( ) . make ( ANNOTATED_OBJECT ) . parameterizedObject ( INSTANCE ) . illegal ( ) ;
    }
}
