///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . dinosaurstraw . DinosaurStrawFactory ;
import java . util . HashMap ;
import java . util . Map ;
import java . util . concurrent . Callable ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . ANNOTATED_OBJECT ;
import static breezemodern . blueautumn . Targets . INSTANCE ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotEquals ;

public final class UnannotatedFunctionalTest
{
    static void test ( final AnnotatedObjectTarget target )
    {
	assertEquals ( INSTANCE , target . unannotated ( INSTANCE ) ) ;
    }

    @ Test
	public void test ( )
    {
	final AnnotatedObjectTarget target = new DinosaurStrawFactory ( ) . make ( ANNOTATED_OBJECT ) ;
	test ( target . annotatedObject ( ) ) ;
    }

    @ Test
	public void testMismap ( ) throws Exception
    {
	final Map < String , Object > parametersMap = new HashMap < String , Object > ( ) ;
	final Object call = new Object ( ) ;
	parametersMap . put ( "call" , call ) ;
	final Object value = new Object ( ) ;
	parametersMap . put ( "value" , value ) ;
	final Callable < ? > callable = new DinosaurStrawFactory ( ) . make ( CallableTarget . class , parametersMap ) ;
	assertNotEquals ( call , callable . call ( ) ) ;
	assertEquals ( value , callable . call ( ) ) ;
    }
}
