///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . easterncrayon . BlueAutumn ;
import org . junit . Test ;

import static java . lang . System . getProperty ;
import static org . junit . Assert . assertFalse ;

public final class BlueAutumnFunctionalTest
{
    private static final String BLUE_AUTUMN = getProperty ( "breezemodern.blueautumn" ) ;

    @ Test
	public void failure ( )
    {
	assertFalse ( "failure" . equals ( BLUE_AUTUMN ) ) ;
    }

    @ Test
	@ BlueAutumn
	public void error ( )
    {
	if ( "error" . equals ( BLUE_AUTUMN ) )
	    {
		throw new RuntimeException ( ) ;
	    }
    }

    @ Test
	@ BlueAutumn
	public void coverage ( )
    {
	if ( ! "coverage" . equals ( BLUE_AUTUMN ) )
	    {
		cover ( ) ;
	    }
    }

    private void cover ( )
    {
    }
}
