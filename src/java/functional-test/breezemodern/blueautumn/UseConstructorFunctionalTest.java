///////////////////////////////////////////////////////////////////////
//
// Copyright © (C) 2014
// Emory Hughes Merryman, III
// emory.merryman@gmail.com
//
// This file is part of dinosaurstraw.
//
// dinosaurstraw is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dinosaurstraw is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

package breezemodern . blueautumn ;

import breezemodern . dinosaurstraw . DinosaurStrawFactory ;
import org . junit . Test ;

import static breezemodern . blueautumn . Targets . ANNOTATED_OBJECT ;
import static breezemodern . blueautumn . Targets . INSTANCE ;
import static org . junit . Assert . assertEquals ;
import static org . junit . Assert . assertNotNull ;
import static org . junit . Assert . assertTrue ;

public final class UseConstructorFunctionalTest
{
    private void test ( final AnnotatedObjectTarget target ) throws Exception
    {
	assertNotNull ( target . constructor ( ) ) ;
	assertTrue ( target . abstractObjectTarget ( ) instanceof ChildObjectTarget ) ;
	assertTrue ( target . callableTarget ( INSTANCE ) instanceof CallableTarget ) ;
	assertNotNull ( target . callableTarget ( INSTANCE ) . call ( ) ) ;
	assertEquals ( INSTANCE , target . callableTarget ( INSTANCE ) . call ( ) ) ;
	UnannotatedFunctionalTest . test ( target ) ;
	UseArrayFunctionalTest . test ( target ) ;
	UseClassConstantFunctionalTest . test ( target ) ;
	UseInstanceMethodFunctionalTest . test ( target ) ;
	UseIntegerConstantFunctionalTest . test ( target ) ;
	UseParameterFunctionalTest . test ( target ) ;
	UseStaticMethodFunctionalTest . test ( target ) ;
    }

    @ Test
	public void test ( ) throws Exception
    {
	assertNotNull ( new DinosaurStrawFactory ( ) . make ( Object . class ) ) ;
	final AnnotatedObjectTarget target = new DinosaurStrawFactory ( ) . make ( ANNOTATED_OBJECT ) ;
	test ( target . annotatedObject ( ) ) ;
    }
}
