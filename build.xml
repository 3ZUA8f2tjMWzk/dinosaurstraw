<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright © (C) 2014
    Emory Hughes Merryman, III
    emory.merryman@gmail.com
-->
<!--
    This file is part of dinosaurstraw.

    dinosaurstraw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    dinosaurstraw is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with dinosaurstraw.  If not, see <http://www.gnu.org/licenses/>.
-->
<project name="dinosaurstraw" basedir="." default="publish" xmlns:ivy="antlib:org.apache.ivy.ant">
  <tempfile property="repository.dir" destdir="${java.io.tmpdir}"/>
  <property name="ivy.install.version" value="2.4.0-rc1" />
  <property name="ivy.jar.file" value="${basedir}/ivy.jar" />
  <available file="${ivy.jar.file}" property="ivy-exists" />
  <target name="download-ivy" unless="ivy-exists">
    <get src="http://repo2.maven.org/maven2/org/apache/ivy/ivy/${ivy.install.version}/ivy-${ivy.install.version}.jar" dest="${ivy.jar.file}" usetimestamp="true"/>
  </target>
  <target name="clean">
    <delete dir="${basedir}/build"/>
  </target>
  <target name="init" depends="download-ivy">
    <taskdef resource="org/apache/ivy/ant/antlib.xml" uri="antlib:org.apache.ivy.ant">
      <classpath>
	<fileset file="${ivy.jar.file}"/>
      </classpath>
    </taskdef>
    <mkdir dir="${repository.dir}"/>
    <ivy:settings file="${basedir}/src/ivy/ivysettings.xml"/>
    <ivy:resolve file="${basedir}/src/ivy/ivy.xml"/>
  </target>
  <target name="checkstyle" depends="init">
    <mkdir dir="${basedir}/build/lib/checkstyle"/>
    <ivy:retrieve pattern="${basedir}/build/lib/checkstyle/[artifact]-[revision](-[classifier]).[ext]" conf="_checkstyle_"/>
    <taskdef resource="checkstyletask.properties">
      <classpath>
        <fileset dir="${basedir}/build/lib/checkstyle"/>
      </classpath>
    </taskdef>
    <mkdir dir="${basedir}/build/hollowlobster"/>
    <ivy:retrieve pattern="${basedir}/build/hollowlobster/[artifact].[ext]"/>
    <checkstyle config="${basedir}/build/hollowlobster/checks.xml">
      <fileset dir="${basedir}/src/java/bootstrap"/>
      <fileset dir="${basedir}/src/java/util"/>
      <fileset dir="${basedir}/src/java/main"/>
      <fileset dir="${basedir}/src/java/functional-test"/>
      <fileset dir="${basedir}/src/java/unit-test"/>
      <fileset dir="${basedir}/src/java/target"/>
      <fileset dir="${basedir}/src/java/static"/>
      <property key="java.header.txt" value="${basedir}/build/hollowlobster/java.header.txt"/>
    </checkstyle>
  </target>
  <target name="target" depends="init">
    <mkdir dir="${basedir}/build/target/lib/easterncrayon"/>
    <ivy:retrieve pattern="${basedir}/build/target/lib/easterncrayon/[artifact]-[revision](-[classifier]).[ext]" conf="_easterncrayon_"/>
    <mkdir dir="${basedir}/build/target/classes"/>
    <javac srcdir="${basedir}/src/java/target" destdir="${basedir}/build/target/classes" debug="true" debuglevel="lines,vars,source">
      <compilerarg line="-g"/>
      <classpath>
	<fileset dir="${basedir}/build/target/lib/easterncrayon"/>
      </classpath>
    </javac>
  </target>
  <target name="functional-test" depends="init,target">
    <mkdir dir="${basedir}/build/functional-test/classes/util"/>
    <javac srcdir="${basedir}/src/java/util" destdir="${basedir}/build/functional-test/classes/util"/>
    <mkdir dir="${basedir}/build/functional-test/lib/javassist"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/javassist/[artifact]-[revision](-[classifier]).[ext]" conf="_javassist_"/>
    <mkdir dir="${basedir}/build/functional-test/lib/paranamer"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/paranamer/[artifact]-[revision](-[classifier]).[ext]" conf="_paranamer_"/>
    <mkdir dir="${basedir}/build/functional-test/lib/guava"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/guava/[artifact]-[revision](-[classifier]).[ext]" conf="_guava_"/>
    <mkdir dir="${basedir}/build/functional-test/lib/easterncrayon"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/easterncrayon/[artifact]-[revision](-[classifier]).[ext]" conf="_easterncrayon_"/>
    <mkdir dir="${basedir}/build/functional-test/lib/junit"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/junit/[artifact]-[revision](-[classifier]).[ext]" conf="_junit_"/>
    <mkdir dir="${basedir}/build/functional-test/lib/redspace"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/redspace/[artifact]-[revision](-[classifier]).[ext]" conf="_redspace_"/>
    <mkdir dir="${basedir}/build/functional-test/classes/uninstrumented"/>
    <javac destdir="${basedir}/build/functional-test/classes/uninstrumented" debug="true" debuglevel="lines,vars,source">
      <src path="${basedir}/src/java/functional-test"/>
      <src path="${basedir}/src/java/main"/>
      <src path="${basedir}/src/java/bootstrap"/>
      <src path="${basedir}/src/java/static"/>
      <classpath>
	<fileset dir="${basedir}/build/functional-test/lib/javassist"/>
	<fileset dir="${basedir}/build/functional-test/lib/paranamer"/>
	<fileset dir="${basedir}/build/functional-test/lib/guava"/>
	<fileset dir="${basedir}/build/functional-test/lib/easterncrayon"/>
	<fileset dir="${basedir}/build/functional-test/lib/redspace"/>
	<fileset dir="${basedir}/build/functional-test/lib/junit"/>
	<dirset dir="${basedir}/build/target/classes"/>
	<dirset dir="${basedir}/build/functional-test/classes/util"/>
      </classpath>
    </javac>
    <mkdir dir="${basedir}/build/functional-test/lib/cobertura"/>
    <ivy:retrieve pattern="${basedir}/build/functional-test/lib/cobertura/[artifact]-[revision](-[classifier]).[ext]" conf="_cobertura_"/>
    <taskdef resource="tasks.properties">
      <classpath>
	<fileset dir="${basedir}/build/functional-test/lib/cobertura"/>
      </classpath>
    </taskdef>
    <mkdir dir="${basedir}/build/functional-test/classes/instrumented"/>
    <cobertura-instrument datafile="${basedir}/build/functional-test/data.ser" todir="${basedir}/build/functional-test/classes/instrumented">
      <fileset dir="${basedir}/build/functional-test/classes/uninstrumented"/>
      <ignoreMethodAnnotation annotationName="breezemodern.easterncrayon.BlueAutumn"/>
    </cobertura-instrument>
    <mkdir dir="${basedir}/build/functional-test/junit"/>
    <junit>
      <sysproperty key="net.sourceforge.cobertura.datafile" file="${basedir}/build/functional-test/data.ser"/>
      <sysproperty key="breezemodern.blueautumn" value="${breezemodern.blueautumn}"/>
      <classpath>
	<dirset dir="${basedir}/build/functional-test/classes/instrumented"/>
	<dirset dir="${basedir}/build/functional-test/classes/uninstrumented"/>
	<fileset dir="${basedir}/build/functional-test/lib/javassist"/>
	<fileset dir="${basedir}/build/functional-test/lib/paranamer"/>
	<fileset dir="${basedir}/build/functional-test/lib/guava"/>
	<fileset dir="${basedir}/build/functional-test/lib/easterncrayon"/>
	<fileset dir="${basedir}/build/functional-test/lib/redspace"/>
	<fileset dir="${basedir}/build/functional-test/lib/junit"/>
	<fileset dir="${basedir}/build/functional-test/lib/cobertura"/>
	<dirset dir="${basedir}/build/target/classes"/>
	<dirset dir="${basedir}/build/functional-test/classes/util"/>
      </classpath>
      <batchtest fork="yes" errorproperty="functional-test.error" failureproperty="functional-test.failure" todir="${basedir}/build/functional-test/junit">
	<fileset dir="${basedir}/src/java/functional-test"/>
	<formatter type="xml"/>
      </batchtest>
      <assertions>
	<enable/>
      </assertions>
    </junit>
    <mkdir dir="${basedir}/build/functional-test/junitreport.xml"/>
    <mkdir dir="${basedir}/build/functional-test/junitreport.html"/>
    <junitreport todir="${basedir}/build/functional-test/junitreport.xml">
      <fileset dir="${basedir}/build/functional-test/junit"/>
      <report todir="${basedir}/build/functional-test/junitreport.html"/>
    </junitreport>
    <mkdir dir="${basedir}/build/functional-test/cobertura-report"/>
    <cobertura-report datafile="${basedir}/build/functional-test/data.ser" destdir="${basedir}/build/functional-test/cobertura-report">
      <fileset dir="${basedir}/src/java/bootstrap"/>
      <fileset dir="${basedir}/src/java/main"/>
      <fileset dir="${basedir}/src/java/functional-test"/>
    </cobertura-report>
    <cobertura-check datafile="${basedir}/build/functional-test/data.ser" haltonfailure="no" failureproperty="functional-test.coverage" linerate="100" branchrate="100" packagelinerate="100" packagebranchrate="100" totallinerate="100" totalbranchrate="100"/>
    <fail message="The functional-tests errored.">
      <condition>
	<isset property="functional-test.error"/>
      </condition>
    </fail>
    <fail message="The functional-tests failed.">
      <condition>
	<isset property="functional-test.failure"/>
      </condition>
    </fail>
    <fail message="The functional-tests did not cover.">
      <condition>
	<isset property="functional-test.coverage"/>
      </condition>
    </fail>
  </target>
  <target name="unit-test" depends="target">
    <mkdir dir="${basedir}/build/unit-test/lib/guava"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/guava/[artifact]-[revision](-[classifier]).[ext]" conf="_guava_"/>
    <mkdir dir="${basedir}/build/unit-test/classes/static"/>
    <javac srcdir="${basedir}/src/java/static" destdir="${basedir}/build/unit-test/classes/static" debug="yes" debuglevel="lines,vars,source">
      <classpath>
	<fileset dir="${basedir}/build/unit-test/lib/guava"/>
      </classpath>
    </javac>
    <mkdir dir="${basedir}/build/unit-test/lib/javassist"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/javassist/[artifact]-[revision](-[classifier]).[ext]" conf="_javassist_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/paranamer"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/paranamer/[artifact]-[revision](-[classifier]).[ext]" conf="_paranamer_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/easterncrayon"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/easterncrayon/[artifact]-[revision](-[classifier]).[ext]" conf="_easterncrayon_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/mockito-all"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/mockito-all/[artifact]-[revision](-[classifier]).[ext]" conf="_mockito-all_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/junit"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/junit/[artifact]-[revision](-[classifier]).[ext]" conf="_junit_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/powermock-api-mockito"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/powermock-api-mockito/[artifact]-[revision](-[classifier]).[ext]" conf="_powermock-api-mockito_"/>
    <mkdir dir="${basedir}/build/unit-test/lib/powermock-module-junit4"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/powermock-module-junit4/[artifact]-[revision](-[classifier]).[ext]" conf="_powermock-module-junit4_"/>
    <mkdir dir="${basedir}/build/unit-test/classes/uninstrumented"/>
    <javac destdir="${basedir}/build/unit-test/classes/uninstrumented" debug="yes" debuglevel="lines,vars,source" source="6" target="6">
      <src path="${basedir}/src/java/bootstrap"/>
      <src path="${basedir}/src/java/main"/>
      <src path="${basedir}/src/java/util"/>
      <src path="${basedir}/src/java/unit-test"/>
      <classpath>
	<fileset dir="${basedir}/build/unit-test/lib/javassist"/>
	<fileset dir="${basedir}/build/unit-test/lib/powermock-api-mockito"/>
	<fileset dir="${basedir}/build/unit-test/lib/paranamer"/>
	<fileset dir="${basedir}/build/unit-test/lib/guava"/>
	<fileset dir="${basedir}/build/unit-test/lib/easterncrayon"/>
	<dirset dir="${basedir}/build/target/classes"/>
	<dirset dir="${basedir}/build/unit-test/classes/static"/>
	<fileset dir="${basedir}/build/unit-test/lib/junit"/>
	<fileset dir="${basedir}/build/unit-test/lib/mockito-all"/>
	<fileset dir="${basedir}/build/unit-test/lib/powermock-api-mockito"/>
	<fileset dir="${basedir}/build/unit-test/lib/powermock-module-junit4"/>
      </classpath>
    </javac>
    <mkdir dir="${basedir}/build/unit-test/lib/cobertura"/>
    <ivy:retrieve pattern="${basedir}/build/unit-test/lib/cobertura/[artifact]-[revision](-[classifier]).[ext]" conf="_cobertura_"/>
    <taskdef resource="tasks.properties">
      <classpath>
	<fileset dir="${basedir}/build/unit-test/lib/cobertura"/>
      </classpath>
    </taskdef>
    <mkdir dir="${basedir}/build/unit-test/classes/instrumented"/>
    <cobertura-instrument datafile="${basedir}/build/unit-test/data.ser" todir="${basedir}/build/unit-test/classes/instrumented">
      <fileset dir="${basedir}/build/unit-test/classes/uninstrumented"/>
      <ignoreMethodAnnotation annotationName="breezemodern.easterncrayon.AggressiveEagle"/>
    </cobertura-instrument>
    <mkdir dir="${basedir}/build/unit-test/junit"/>
    <junit>
      <sysproperty key="net.sourceforge.cobertura.datafile" file="${basedir}/build/unit-test/data.ser"/>
      <sysproperty key="breezemodern.aggressiveeagle" value="${breezemodern.aggressiveeagle}"/>
      <classpath>
	<dirset dir="${basedir}/build/unit-test/classes/instrumented"/>
	<dirset dir="${basedir}/build/unit-test/classes/uninstrumented"/>
	<fileset dir="${basedir}/build/unit-test/lib/javassist"/>
	<fileset dir="${basedir}/build/unit-test/lib/paranamer"/>
	<fileset dir="${basedir}/build/unit-test/lib/guava"/>
	<fileset dir="${basedir}/build/unit-test/lib/easterncrayon"/>
	<dirset dir="${basedir}/build/target/classes"/>
	<dirset dir="${basedir}/build/unit-test/classes/static"/>
	<fileset dir="${basedir}/build/unit-test/lib/junit"/>
	<fileset dir="${basedir}/build/unit-test/lib/cobertura"/>
	<fileset dir="${basedir}/build/unit-test/lib/mockito-all"/>
	<fileset dir="${basedir}/build/unit-test/lib/powermock-api-mockito"/>
	<fileset dir="${basedir}/build/unit-test/lib/powermock-module-junit4"/>
      </classpath>
      <batchtest fork="yes" errorproperty="unit-test.error" failureproperty="unit-test.failure" todir="${basedir}/build/unit-test/junit">
	<fileset dir="${basedir}/src/java/unit-test"/>
	<formatter type="xml"/>
      </batchtest>
      <assertions>
	<enable/>
      </assertions>
     </junit>
     <mkdir dir="${basedir}/build/unit-test/junitreport.xml"/>
     <mkdir dir="${basedir}/build/unit-test/junitreport.html"/>
     <junitreport todir="${basedir}/build/unit-test/junitreport.xml">
       <fileset dir="${basedir}/build/unit-test/junit"/>
       <report todir="${basedir}/build/unit-test/junitreport.html"/>
     </junitreport>
     <mkdir dir="${basedir}/build/unit-test/cobertura-report"/>
     <cobertura-report datafile="${basedir}/build/unit-test/data.ser" destdir="${basedir}/build/unit-test/cobertura-report">
       <fileset dir="${basedir}/src/java/bootstrap"/>
       <fileset dir="${basedir}/src/java/main"/>
       <fileset dir="${basedir}/src/java/unit-test"/>
       <fileset dir="${basedir}/src/java/util"/>
     </cobertura-report>
     <cobertura-check datafile="${basedir}/build/unit-test/data.ser" haltonfailure="no" failureproperty="unit-test.coverage" linerate="100" branchrate="100" packagelinerate="100" packagebranchrate="100" totallinerate="100" totalbranchrate="100"/>
     <fail message="The unit-tests errored.">
       <condition>
	 <isset property="unit-test.error"/>
       </condition>
     </fail>
     <fail message="The unit-tests failed.">
       <condition>
	 <isset property="unit-test.failure"/>
       </condition>
     </fail>
     <fail message="The unit-tests did not cover.">
       <condition>
	 <isset property="unit-test.coverage"/>
       </condition>
     </fail>
  </target>
  <target name="compile" depends="init">
    <mkdir dir="${basedir}/build/compile/lib/javassist"/>
    <ivy:retrieve pattern="${basedir}/build/compile/lib/javassist/[artifact]-[revision](-[classifier]).[ext]" conf="_javassist_"/>
    <mkdir dir="${basedir}/build/compile/lib/paranamer"/>
    <ivy:retrieve pattern="${basedir}/build/compile/lib/paranamer/[artifact]-[revision](-[classifier]).[ext]" conf="_paranamer_"/>
    <mkdir dir="${basedir}/build/compile/lib/guava"/>
    <ivy:retrieve pattern="${basedir}/build/compile/lib/guava/[artifact]-[revision](-[classifier]).[ext]" conf="_guava_"/>
    <mkdir dir="${basedir}/build/compile/lib/easterncrayon"/>
    <ivy:retrieve pattern="${basedir}/build/compile/lib/easterncrayon/[artifact]-[revision](-[classifier]).[ext]" conf="_easterncrayon_"/>
    <mkdir dir="${basedir}/build/compile/classes"/>
    <javac destdir="${basedir}/build/compile/classes" debug="true" debuglevel="lines,vars,source">
      <src path="${basedir}/src/java/util"/>
      <src path="${basedir}/src/java/main"/>
      <src path="${basedir}/src/java/bootstrap"/>
      <src path="${basedir}/src/java/static"/>
      <classpath>
	<fileset dir="${basedir}/build/compile/lib/javassist"/>
	<fileset dir="${basedir}/build/compile/lib/paranamer"/>
	<fileset dir="${basedir}/build/compile/lib/guava"/>
	<fileset dir="${basedir}/build/compile/lib/easterncrayon"/>
      </classpath>
    </javac>
    <mkdir dir="${basedir}/build/publish"/>
    <jar destfile="${basedir}/build/publish/${ant.project.name}.jar" compress="yes" level="9" basedir="${basedir}/build/compile/classes"/>
  </target>
  <target name="publish" depends="compile">
    <exec executable="git" output="${basedir}/build/git.rev-list.txt">
      <arg value="rev-list"/>
      <arg value="HEAD"/>
    </exec>
    <resourcecount property="version.minor">
      <tokens>
	<concat>
	  <filterchain>
	    <tokenfilter>
	      <linetokenizer/>
	    </tokenfilter>
	  </filterchain>
	  <fileset file="${basedir}/build/git.rev-list.txt"/>
	</concat>
      </tokens>
    </resourcecount>
    <mkdir dir="${basedir}/build"/>
    <exec executable="git" output="${basedir}/build/git.rev-list.txt">
      <arg value="rev-list"/>
      <arg value="HEAD"/>
    </exec>
    <exec executable="wc" outputproperty="version.minor">
      <arg value="${basedir}/build/git.rev-list.txt"/>
      <arg value="-l"/>
    </exec>
    <ivy:publish resolver="internal" pubrevision="1.${version.minor}">
      <artifacts pattern="${basedir}/build/publish/[artifact].[ext]"/>
    </ivy:publish>
  </target>
</project>